# example of env variables to use the app with a local seed

export ANDROID_APP_NAME="Acceslibre Mobilités Local"
export ANDROID_APP_ID="fr.acceslibremobilites.local"

# The seed has no HTTPS. Allow insecure requests
export OIDC_INSECURE=true

# All services will be accessed by wifi in your local network.
# you need to know your private IP in this network
# ubuntu : hostname -I

# Web root url displayed to the user
# we set the url of the main frontend
export WEB_ROOT="http://192.168.1.11:1234"

# OSM API
export API_ROOT=http://192.168.1.11:3000

export OIDC_SERVER=http://192.168.1.11:3030/realms/yukaimaps

# this is the default client configured in the seed.
# /!\ The clien need to authorize a special redirect uri :
# `org.yukaimaps://oidc`
export OAUTH_CLIENT_ID=local-id

# Photos upload service url
export PHOTO_SERVICE=http://192.168.1.11:8000/photos

# Graph MVT stream
export GRAPH_MVT_URL='http://192.168.1.11:8081/?map=/etc/mapserver/mvt.map&mode=tile&tilemode=gmap&layers=all&tile={x}+{y}+{z}'

# URL for a file containing banned app versions
export BANNED_VERSIONS=""

