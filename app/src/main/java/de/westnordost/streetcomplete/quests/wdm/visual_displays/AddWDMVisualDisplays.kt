package de.westnordost.streetcomplete.quests.wdm.visual_displays

import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.data.osm.geometry.ElementGeometry
import de.westnordost.streetcomplete.data.osm.osmquests.OsmFilterQuestType
import de.westnordost.streetcomplete.data.user.achievements.EditTypeAchievement.WHEELCHAIR
import de.westnordost.streetcomplete.osm.Tags

class AddWDMVisualDisplays : OsmFilterQuestType<WDMVisualDisplays>() {

    override val elementFilter = """
        nodes, ways with PointOfInterest = StopPlace
        and !VisualDisplays
    """
    override val changesetComment = "WDM Survey visual displays"
    override val wikiLink = null
    override val icon = R.drawable.ic_wdm_visual_displays
    override val achievements = listOf(WHEELCHAIR)

    override fun getTitle(tags: Map<String, String>) = R.string.quest_wdm_visual_displays_title

    override fun createForm() = AddWDMVisualDisplaysForm()

    override fun applyAnswerTo(answer: WDMVisualDisplays, tags: Tags, geometry: ElementGeometry, timestampEdited: Long) {
        tags["VisualDisplays"] = answer.osmValue
    }
}
