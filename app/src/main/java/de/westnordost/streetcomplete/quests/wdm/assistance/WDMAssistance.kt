package de.westnordost.streetcomplete.quests.wdm.assistance

/** All assistance:* keys known to StreetComplete */
enum class WDMAssistance(val value: String) {
    BOARDING("boarding"),
    WHEELCHAIR("wheelchair"),
    CHILD("child"),
    INFORMATION("information"),
    STATION_MASTER("station_master"),
    NONE("none");

    companion object {
        val selectableValues = listOf(
            BOARDING, WHEELCHAIR, CHILD, INFORMATION, STATION_MASTER, NONE
        )
    }
}
