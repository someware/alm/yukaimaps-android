package de.westnordost.streetcomplete.quests.wdm.wheelchair_access

import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.data.osm.geometry.ElementGeometry
import de.westnordost.streetcomplete.data.osm.osmquests.OsmFilterQuestType
import de.westnordost.streetcomplete.data.user.achievements.EditTypeAchievement.WHEELCHAIR
import de.westnordost.streetcomplete.osm.Tags

class AddWDMWheelchairAccessParkingBayLimitedNote : OsmFilterQuestType<String>() {

    override val elementFilter = """
        nodes, ways with ParkingBay = Disabled
        and WheelchairAccess = limited
        and !WheelchairAccess:Description
    """
    override val changesetComment = "WDM Survey limited note wheelchair accessibility of parking bay"
    override val wikiLink = null
    override val icon = R.drawable.ic_wdm_wheelchair_parkingbay
    override val achievements = listOf(WHEELCHAIR)

    override fun getTitle(tags: Map<String, String>) = R.string.quest_wdm_wheelchairAccess_parkingBay_limited_note_title

    override fun createForm() = AddWDMWheelchairAccessParkingBayLimitedNoteForm()

    override fun applyAnswerTo(answer: String, tags: Tags, geometry: ElementGeometry, timestampEdited: Long) {
        tags["WheelchairAccess:Description"] = answer
    }
}
