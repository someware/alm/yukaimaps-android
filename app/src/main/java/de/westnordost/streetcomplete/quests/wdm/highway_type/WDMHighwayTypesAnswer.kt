package de.westnordost.streetcomplete.quests.wdm.highway_type

sealed interface WDMHighwayTypesAnswer

data class WDMHighwayTypes(val highwayTypes: List<WDMHighwayType>) : WDMHighwayTypesAnswer
