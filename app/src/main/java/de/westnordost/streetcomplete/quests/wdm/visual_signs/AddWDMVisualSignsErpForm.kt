package de.westnordost.streetcomplete.quests.wdm.visual_signs

import de.westnordost.streetcomplete.R

class AddWDMVisualSignsErpForm : WDMVisualSignsForm() {
    override val contentLayoutResId = R.layout.quest_wdm_visual_signs_erp_explanation
}
