package de.westnordost.streetcomplete.quests.wdm.audible_signals

import de.westnordost.streetcomplete.R

class AddWDMAudibleSignalsHallForm : WDMAudibleSignalsForm() {
    override val contentLayoutResId = R.layout.quest_wdm_audible_signals_hall_explanation
}
