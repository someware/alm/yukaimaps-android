package de.westnordost.streetcomplete.quests.wdm.visual_displays

import de.westnordost.streetcomplete.R

class AddWDMVisualDisplaysForm : WDMVisualDisplaysForm() {
    override val contentLayoutResId = R.layout.quest_wdm_visual_displays_explanation
}
