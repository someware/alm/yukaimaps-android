package de.westnordost.streetcomplete.quests.wdm.wheelchair_access

import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.data.osm.geometry.ElementGeometry
import de.westnordost.streetcomplete.data.osm.osmquests.OsmFilterQuestType
import de.westnordost.streetcomplete.data.user.achievements.EditTypeAchievement.WHEELCHAIR
import de.westnordost.streetcomplete.osm.Tags
import de.westnordost.streetcomplete.osm.updateWithCheckDate

class AddWDMWheelchairAccessEntrance : OsmFilterQuestType<WDMWheelchairAccess>() {

    override val elementFilter = """
        nodes with Entrance
        and !WheelchairAccess
    """
    override val changesetComment = "WDM Survey wheelchair accessibility of entrance"
    override val wikiLink = null
    override val icon = R.drawable.ic_wdm_wheelchair_entrance
    override val achievements = listOf(WHEELCHAIR)

    override fun getTitle(tags: Map<String, String>) = R.string.quest_wdm_wheelchairAccess_entrance_title

    override fun createForm() = AddWDMWheelchairAccessEntranceForm()

    override fun applyAnswerTo(answer: WDMWheelchairAccess, tags: Tags, geometry: ElementGeometry, timestampEdited: Long) {
        tags["WheelchairAccess"] = answer.osmValue
    }
}
