package de.westnordost.streetcomplete.quests.wdm.tilt

import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.data.osm.geometry.ElementGeometry
import de.westnordost.streetcomplete.data.osm.osmquests.OsmFilterQuestType
import de.westnordost.streetcomplete.data.user.achievements.EditTypeAchievement.WHEELCHAIR
import de.westnordost.streetcomplete.osm.Tags

class AddWDMTilt : OsmFilterQuestType<Int>() {

    override val elementFilter = """
        ways with SitePathLink
        and SitePathLink !~ Stairs|Escalator
        and !Tilt
    """

    override val changesetComment = "WDM Specify tilt"
    override val wikiLink = null
    override val icon = R.drawable.ic_quest_wdm_tilt
    override val achievements = listOf(WHEELCHAIR)

    override fun getTitle(tags: Map<String, String>) = R.string.quest_wdm_tilt_title

    override fun createForm() = AddWDMTiltForm()

    override fun applyAnswerTo(answer: Int, tags: Tags, geometry: ElementGeometry, timestampEdited: Long) {
        tags["Tilt"] = answer.toString()
    }
}
