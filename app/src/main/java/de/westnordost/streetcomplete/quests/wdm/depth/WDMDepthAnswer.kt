package de.westnordost.streetcomplete.quests.wdm.depth

import de.westnordost.streetcomplete.osm.Length

data class WDMDepthAnswer(val width: Length, val isARMeasurement: Boolean)
