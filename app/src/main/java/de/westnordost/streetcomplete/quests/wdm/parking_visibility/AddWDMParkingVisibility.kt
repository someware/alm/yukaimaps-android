package de.westnordost.streetcomplete.quests.wdm.parking_visibility

import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.data.elementfilter.toElementFilterExpression
import de.westnordost.streetcomplete.data.osm.geometry.ElementGeometry
import de.westnordost.streetcomplete.data.osm.mapdata.Element
import de.westnordost.streetcomplete.data.osm.mapdata.MapDataWithGeometry
import de.westnordost.streetcomplete.data.osm.mapdata.filter
import de.westnordost.streetcomplete.data.osm.osmquests.OsmFilterQuestType
import de.westnordost.streetcomplete.data.user.achievements.EditTypeAchievement.CITIZEN
import de.westnordost.streetcomplete.data.user.achievements.EditTypeAchievement.WHEELCHAIR
import de.westnordost.streetcomplete.osm.Tags
import de.westnordost.streetcomplete.quests.wdm.parking_visibility.WDMParkingVisibility.UNMARKED
import de.westnordost.streetcomplete.quests.wdm.parking_visibility.WDMParkingVisibility.SIGNAGEONLY
import de.westnordost.streetcomplete.quests.wdm.parking_visibility.WDMParkingVisibility.MARKINGONLY
import de.westnordost.streetcomplete.quests.wdm.parking_visibility.WDMParkingVisibility.DOCKS
import de.westnordost.streetcomplete.quests.wdm.parking_visibility.WDMParkingVisibility.DEMARCATED

class AddWDMParkingVisibility : OsmFilterQuestType<WDMParkingVisibility>() {

    override val elementFilter = "nodes, ways with ParkingBay = Disabled and !ParkingVisibility"
    override val changesetComment = "Specify type of parking visibility"
    override val wikiLink = null
    override val icon = R.drawable.ic_quest_wdm_parking_visibility
    override val achievements = listOf(WHEELCHAIR)

    override fun getTitle(tags: Map<String, String>) = R.string.quest_wdm_parking_visibility_title

    override fun createForm() = AddWDMParkingVisibilityForm()

    override fun applyAnswerTo(answer: WDMParkingVisibility, tags: Tags, geometry: ElementGeometry, timestampEdited: Long) {
        when (answer) {
            UNMARKED -> {
                tags["ParkingVisibility"] = "Unmarked"
            }
            SIGNAGEONLY -> {
                tags["ParkingVisibility"] = "SignageOnly"
            }
            MARKINGONLY -> {
                tags["ParkingVisibility"] = "MarkingOnly"
            }
            DOCKS -> {
                tags["ParkingVisibility"] = "Docks"
            }
            DEMARCATED -> {
                tags["ParkingVisibility"] = "Demarcated"
            }
        }
    }
}
