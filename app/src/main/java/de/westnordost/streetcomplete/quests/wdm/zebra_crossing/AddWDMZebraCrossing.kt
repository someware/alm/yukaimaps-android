package de.westnordost.streetcomplete.quests.wdm.zebra_crossing

import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.data.elementfilter.toElementFilterExpression
import de.westnordost.streetcomplete.data.osm.geometry.ElementGeometry
import de.westnordost.streetcomplete.data.osm.mapdata.Element
import de.westnordost.streetcomplete.data.osm.mapdata.MapDataWithGeometry
import de.westnordost.streetcomplete.data.osm.mapdata.filter
import de.westnordost.streetcomplete.data.osm.osmquests.OsmFilterQuestType
import de.westnordost.streetcomplete.data.user.achievements.EditTypeAchievement.CITIZEN
import de.westnordost.streetcomplete.data.user.achievements.EditTypeAchievement.WHEELCHAIR
import de.westnordost.streetcomplete.osm.Tags
import de.westnordost.streetcomplete.quests.wdm.zebra_crossing.WDMZebraCrossing.NONE
import de.westnordost.streetcomplete.quests.wdm.zebra_crossing.WDMZebraCrossing.GOOD
import de.westnordost.streetcomplete.quests.wdm.zebra_crossing.WDMZebraCrossing.WORN
import de.westnordost.streetcomplete.quests.wdm.zebra_crossing.WDMZebraCrossing.DISCOMFORTABLE
import de.westnordost.streetcomplete.quests.wdm.zebra_crossing.WDMZebraCrossing.HAZARDOUS

class AddWDMZebraCrossing : OsmFilterQuestType<WDMZebraCrossing>() {

    override val elementFilter = "ways with SitePathLink and SitePathLink = Crossing and !ZebraCrossing"
    override val changesetComment = "Specify type of zebra crossing"
    override val wikiLink = null
    override val icon = R.drawable.ic_quest_wdm_zebra_crossing
    override val achievements = listOf(WHEELCHAIR)

    override fun getTitle(tags: Map<String, String>) = R.string.quest_wdm_zebra_crossing_title

    override fun createForm() = AddWDMZebraCrossingForm()

    override fun applyAnswerTo(answer: WDMZebraCrossing, tags: Tags, geometry: ElementGeometry, timestampEdited: Long) {
        when (answer) {
            NONE -> {
                tags["ZebraCrossing"] = "None"
            }
            GOOD -> {
                tags["ZebraCrossing"] = "Good"
            }
            WORN -> {
                tags["ZebraCrossing"] = "Worn"
            }
            DISCOMFORTABLE -> {
                tags["ZebraCrossing"] = "Discomfortable"
            }
            HAZARDOUS -> {
                tags["ZebraCrossing"] = "Hazardous"
            }
        }
    }
}
