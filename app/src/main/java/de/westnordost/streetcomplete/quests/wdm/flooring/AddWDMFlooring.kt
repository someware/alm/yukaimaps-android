package de.westnordost.streetcomplete.quests.wdm.flooring

import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.data.elementfilter.toElementFilterExpression
import de.westnordost.streetcomplete.data.osm.geometry.ElementGeometry
import de.westnordost.streetcomplete.data.osm.mapdata.Element
import de.westnordost.streetcomplete.data.osm.mapdata.MapDataWithGeometry
import de.westnordost.streetcomplete.data.osm.mapdata.filter
import de.westnordost.streetcomplete.data.osm.osmquests.OsmFilterQuestType
import de.westnordost.streetcomplete.data.user.achievements.EditTypeAchievement.CITIZEN
import de.westnordost.streetcomplete.data.user.achievements.EditTypeAchievement.WHEELCHAIR
import de.westnordost.streetcomplete.osm.Tags
import de.westnordost.streetcomplete.quests.wdm.flooring.WDMFlooring.NONE
import de.westnordost.streetcomplete.quests.wdm.flooring.WDMFlooring.GOOD
import de.westnordost.streetcomplete.quests.wdm.flooring.WDMFlooring.WORN
import de.westnordost.streetcomplete.quests.wdm.flooring.WDMFlooring.DISCOMFORTABLE
import de.westnordost.streetcomplete.quests.wdm.flooring.WDMFlooring.HAZARDOUS

class AddWDMFlooring : OsmFilterQuestType<WDMFlooring>() {

    override val elementFilter = "ways with SitePathLink and SitePathLink !~ Stairs|Escalator and !Flooring:Status"
    override val changesetComment = "Specify type of flooring"
    override val wikiLink = null
    override val icon = R.drawable.ic_quest_wdm_flooring_material
    override val achievements = listOf(WHEELCHAIR)

    override fun getTitle(tags: Map<String, String>) = R.string.quest_wdm_flooring_title

    override fun createForm() = AddWDMFlooringForm()

    override fun applyAnswerTo(answer: WDMFlooring, tags: Tags, geometry: ElementGeometry, timestampEdited: Long) {
        when (answer) {
            NONE -> {
                tags["Flooring:Status"] = "None"
            }
            GOOD -> {
                tags["Flooring:Status"] = "Good"
            }
            WORN -> {
                tags["Flooring:Status"] = "Worn"
            }
            DISCOMFORTABLE -> {
                tags["Flooring:Status"] = "Discomfortable"
            }
            HAZARDOUS -> {
                tags["Flooring:Status"] = "Hazardous"
            }
        }
    }
}
