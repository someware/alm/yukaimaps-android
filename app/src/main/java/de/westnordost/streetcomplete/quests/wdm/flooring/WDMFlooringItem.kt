package de.westnordost.streetcomplete.quests.wdm.flooring

import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.quests.wdm.flooring.WDMFlooring.NONE
import de.westnordost.streetcomplete.quests.wdm.flooring.WDMFlooring.GOOD
import de.westnordost.streetcomplete.quests.wdm.flooring.WDMFlooring.WORN
import de.westnordost.streetcomplete.quests.wdm.flooring.WDMFlooring.DISCOMFORTABLE
import de.westnordost.streetcomplete.quests.wdm.flooring.WDMFlooring.HAZARDOUS
import de.westnordost.streetcomplete.view.image_select.Item

fun WDMFlooring.asItem() = Item(this, iconResId, titleResId)

private val WDMFlooring.titleResId: Int get() = when (this) {
    NONE ->  R.string.quest_wdm_flooring_none
    GOOD -> R.string.quest_wdm_flooring_good
    WORN ->      R.string.quest_wdm_flooring_worn
    DISCOMFORTABLE ->      R.string.quest_wdm_flooring_discomfortable
    HAZARDOUS ->      R.string.quest_wdm_flooring_hazardous
}

private val WDMFlooring.iconResId: Int get() = when (this) {
    NONE ->  R.drawable.wdm_default_photo
    GOOD -> R.drawable.wdm_flooring_good
    WORN ->      R.drawable.wdm_flooring_worn
    DISCOMFORTABLE ->      R.drawable.wdm_flooring_discomfortable
    HAZARDOUS ->      R.drawable.wdm_flooring_hazardous
}
