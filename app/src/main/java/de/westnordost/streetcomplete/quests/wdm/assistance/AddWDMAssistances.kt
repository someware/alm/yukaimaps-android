package de.westnordost.streetcomplete.quests.wdm.assistance

import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.data.elementfilter.toElementFilterExpression
import de.westnordost.streetcomplete.data.osm.geometry.ElementGeometry
import de.westnordost.streetcomplete.data.osm.mapdata.Element
import de.westnordost.streetcomplete.data.osm.mapdata.MapDataWithGeometry
import de.westnordost.streetcomplete.data.osm.osmquests.OsmElementQuestType
import de.westnordost.streetcomplete.data.user.achievements.EditTypeAchievement.WHEELCHAIR
import de.westnordost.streetcomplete.osm.Tags

class AddWDMAssistances : OsmElementQuestType<WDMAssistancesAnswer> {

    private val filter by lazy { """
        nodes, ways with PointOfInterest = StopPlace
    """.toElementFilterExpression() }

    override val changesetComment = "Specify what kind of assistance"
    override val wikiLink = null
    override val icon = R.drawable.ic_quest_wdm_assistance
    override val isDeleteElementEnabled = true
    override val achievements = listOf(WHEELCHAIR)

    override fun getTitle(tags: Map<String, String>) = R.string.quest_wdm_assistance_title

    override fun getApplicableElements(mapData: MapDataWithGeometry): Iterable<Element> =
        mapData.nodes.filter { isApplicableTo(it) }

    override fun isApplicableTo(element: Element): Boolean =
        /* Only recycling containers that do either not have any recycling:* tag yet or
         * haven't been touched for 2 years and are exclusively recycling types selectable in
         * StreetComplete. */
        filter.matches(element) &&
            !element.hasAnyAssistances()

    override fun createForm() = AddWDMAssistancesForm()

    override fun applyAnswerTo(answer: WDMAssistancesAnswer, tags: Tags, geometry: ElementGeometry, timestampEdited: Long) {
        if (answer is WDMAssistances) {
            applyWDMAssistanceAnswer(answer.assistances, tags)
        }
    }

    private fun applyWDMAssistanceAnswer(assistances: List<WDMAssistance>, tags: Tags) {
        // set selected Assistance:* taggings to "yes"
        val selectedAssistances = assistances.map { "Assistance:${it.value}" }
        for (assistance in selectedAssistances) {
            tags[assistance] = "yes"
        }
    }

    private fun Element.hasAnyAssistances(): Boolean =
        tags.any { it.key.startsWith("Assistance:") && it.value == "yes" }
}
