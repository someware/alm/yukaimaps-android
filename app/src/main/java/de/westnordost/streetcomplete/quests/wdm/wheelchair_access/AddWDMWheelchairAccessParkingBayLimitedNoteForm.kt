package de.westnordost.streetcomplete.quests.wdm.wheelchair_access

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.core.widget.doAfterTextChanged
import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.databinding.QuestWdmWheelchairParkingbayLimitedNoteBinding
import de.westnordost.streetcomplete.quests.AbstractOsmQuestForm
import de.westnordost.streetcomplete.util.ktx.nonBlankTextOrNull

class AddWDMWheelchairAccessParkingBayLimitedNoteForm : AbstractOsmQuestForm<String>() {

    override val contentLayoutResId = R.layout.quest_wdm_wheelchair_parkingbay_limited_note
    private val binding by contentViewBinding(QuestWdmWheelchairParkingbayLimitedNoteBinding::bind)

    private val noteText: String? get() = binding.noteInput.nonBlankTextOrNull

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.noteInput.doAfterTextChanged { checkIsFormComplete() }
    }

    override fun isFormComplete() = noteText != null

    override fun onClickOk() {
        applyAnswer(noteText.toString())
    }

    companion object {
        private const val ARG_DESCRIPTION = "description"

        fun create(descriptionResId: Int): AddWDMWheelchairAccessParkingBayLimitedNoteForm {
            val form = AddWDMWheelchairAccessParkingBayLimitedNoteForm()
            form.arguments = bundleOf(ARG_DESCRIPTION to descriptionResId)
            return form
        }
    }
}
