package de.westnordost.streetcomplete.quests.wdm.audible_signals

import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.quests.AbstractOsmQuestForm
import de.westnordost.streetcomplete.quests.AnswerItem
import de.westnordost.streetcomplete.quests.wdm.audible_signals.WDMAudibleSignals.LIMITED
import de.westnordost.streetcomplete.quests.wdm.audible_signals.WDMAudibleSignals.NO
import de.westnordost.streetcomplete.quests.wdm.audible_signals.WDMAudibleSignals.YES

open class WDMAudibleSignalsForm : AbstractOsmQuestForm<WDMAudibleSignals>() {

    override val buttonPanelAnswers = listOf(
        AnswerItem(R.string.quest_generic_hasFeature_no) { applyAnswer(NO) },
        AnswerItem(R.string.quest_generic_hasFeature_yes) { applyAnswer(YES) },
        AnswerItem(R.string.quest_wheelchairAccess_limited) {
            applyAnswer(LIMITED)
        },
    )
}
