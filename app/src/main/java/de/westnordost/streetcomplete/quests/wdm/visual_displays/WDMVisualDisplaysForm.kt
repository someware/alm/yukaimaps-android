package de.westnordost.streetcomplete.quests.wdm.visual_displays

import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.quests.AbstractOsmQuestForm
import de.westnordost.streetcomplete.quests.AnswerItem
import de.westnordost.streetcomplete.quests.wdm.visual_displays.WDMVisualDisplays.ADAPTED
import de.westnordost.streetcomplete.quests.wdm.visual_displays.WDMVisualDisplays.UNADAPTED
import de.westnordost.streetcomplete.quests.wdm.visual_displays.WDMVisualDisplays.NO

open class WDMVisualDisplaysForm : AbstractOsmQuestForm<WDMVisualDisplays>() {

    override val buttonPanelAnswers = listOf(
        AnswerItem(R.string.quest_generic_hasFeature_no) { applyAnswer(NO) },
        AnswerItem(R.string.quest_wdm_audio_information_adapted) { applyAnswer(ADAPTED) },
        AnswerItem(R.string.quest_wdm_audio_information_unadapted) {
            applyAnswer(UNADAPTED)
        },
    )
}
