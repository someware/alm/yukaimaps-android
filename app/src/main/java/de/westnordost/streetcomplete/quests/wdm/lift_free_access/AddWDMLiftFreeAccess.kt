package de.westnordost.streetcomplete.quests.wdm.escalator_free_access

import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.data.osm.geometry.ElementGeometry
import de.westnordost.streetcomplete.data.osm.osmquests.OsmFilterQuestType
import de.westnordost.streetcomplete.data.quest.AllCountriesExcept
import de.westnordost.streetcomplete.data.user.achievements.EditTypeAchievement
import de.westnordost.streetcomplete.data.user.achievements.EditTypeAchievement.CITIZEN
import de.westnordost.streetcomplete.osm.Tags
import de.westnordost.streetcomplete.quests.YesNoQuestForm
import de.westnordost.streetcomplete.util.ktx.toYesNo

class AddWDMEscalatorFreeAccess : OsmFilterQuestType<Boolean>() {

    override val elementFilter = """
        nodes, ways with PointOfInterest = StopPlace
         and !EscalatorFreeAccess
    """

    override val changesetComment = "Specify escalator free access"
    override val wikiLink = null
    override val icon = R.drawable.ic_quest_wdm_default_poi
    override val achievements = listOf(EditTypeAchievement.WHEELCHAIR)

    override fun getTitle(tags: Map<String, String>) = R.string.quest_wdm_escalator_free_access_title

    override fun createForm() = YesNoQuestForm()

    override fun applyAnswerTo(answer: Boolean, tags: Tags, geometry: ElementGeometry, timestampEdited: Long) {
        tags["EscalatorFreeAccess"] = answer.toYesNo()
    }
}
