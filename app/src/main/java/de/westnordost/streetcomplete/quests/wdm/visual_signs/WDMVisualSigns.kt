package de.westnordost.streetcomplete.quests.wdm.visual_signs

enum class WDMVisualSigns(val osmValue: String) {
    YES("yes"),
    LIMITED("limited"),
    NO("no"),
}
