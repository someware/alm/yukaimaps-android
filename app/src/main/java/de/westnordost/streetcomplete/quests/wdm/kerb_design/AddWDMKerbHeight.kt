package de.westnordost.streetcomplete.quests.wdm.kerb_design

import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.data.elementfilter.toElementFilterExpression
import de.westnordost.streetcomplete.data.osm.geometry.ElementGeometry
import de.westnordost.streetcomplete.data.osm.mapdata.Element
import de.westnordost.streetcomplete.data.osm.mapdata.MapDataWithGeometry
import de.westnordost.streetcomplete.data.osm.osmquests.OsmElementQuestType
import de.westnordost.streetcomplete.quests.kerb_height.KerbHeight
import de.westnordost.streetcomplete.data.user.achievements.EditTypeAchievement.BICYCLIST
import de.westnordost.streetcomplete.data.user.achievements.EditTypeAchievement.BLIND
import de.westnordost.streetcomplete.data.user.achievements.EditTypeAchievement.WHEELCHAIR
import de.westnordost.streetcomplete.osm.Tags
import de.westnordost.streetcomplete.osm.updateWithCheckDate
import de.westnordost.streetcomplete.quests.kerb_height.AddKerbHeightForm
import java.util.HashSet

class AddWDMKerbHeight : OsmElementQuestType<KerbHeight> {

    private val crossingFilter by lazy {"""
        nodes with PathJunction and PathJunction = Crossing and !KerbHeight
        and KerbDesign ~ Raised|Lowered|KerbCut
    """.toElementFilterExpression() }

    override val changesetComment = "Determine the heights of kerbs at crossings"
    override val wikiLink = null
    override val icon = R.drawable.ic_quest_wdm_default_sitepathlink
    override val achievements = listOf(WHEELCHAIR)

    override fun getTitle(tags: Map<String, String>) = R.string.quest_wdm_kerb_height_title

    override fun getApplicableElements(mapData: MapDataWithGeometry): Iterable<Element> {
        return mapData.nodes
            .filter { crossingFilter.matches(it) }
    }

    override fun isApplicableTo(element: Element): Boolean? =
        if (!crossingFilter.matches(element)) false else null

    override fun createForm() = AddKerbHeightForm()

    override fun applyAnswerTo(
        answer: KerbHeight,
        tags: Tags,
        geometry: ElementGeometry,
        timestampEdited: Long
    ) {
        tags["KerbHeight"] = answer.osmValue
    }
}
