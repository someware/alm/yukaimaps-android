package de.westnordost.streetcomplete.quests.wdm.visual_displays

import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.data.osm.geometry.ElementGeometry
import de.westnordost.streetcomplete.data.osm.osmquests.OsmFilterQuestType
import de.westnordost.streetcomplete.data.quest.AllCountriesExcept
import de.westnordost.streetcomplete.data.user.achievements.EditTypeAchievement
import de.westnordost.streetcomplete.data.user.achievements.EditTypeAchievement.CITIZEN
import de.westnordost.streetcomplete.osm.Tags
import de.westnordost.streetcomplete.quests.YesNoQuestForm
import de.westnordost.streetcomplete.util.ktx.toYesNo

class AddWDMElevatorVisualDisplays : OsmFilterQuestType<Boolean>() {

    override val elementFilter = """
        nodes, ways with SitePathLink = Elevator
         and !VisualDisplays
    """

    override val changesetComment = "Specify elevator visual displays"
    override val wikiLink = null
    override val icon = R.drawable.ic_quest_wdm_elevator_visual_displays
    override val achievements = listOf(EditTypeAchievement.WHEELCHAIR)

    override fun getTitle(tags: Map<String, String>) = R.string.quest_wdm_elevator_visual_displays_title

    override fun createForm() = YesNoQuestForm()

    override fun applyAnswerTo(answer: Boolean, tags: Tags, geometry: ElementGeometry, timestampEdited: Long) {
        tags["VisualDisplays"] = answer.toYesNo()
    }
}
