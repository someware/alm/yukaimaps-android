package de.westnordost.streetcomplete.quests.wdm.kerb_design

enum class WDMKerbDesign {
    NONE,
    RAISED,
    LOWERED,
    FLUSHED,
    KERBCUT
}
