package de.westnordost.streetcomplete.quests.wdm.visual_displays

enum class WDMVisualDisplays(val osmValue: String) {
    ADAPTED("adapted"),
    UNADAPTED("unadapted"),
    NO("no"),
}
