package de.westnordost.streetcomplete.quests.wdm.lighting

/** All Lighting:* keys known to StreetComplete */
enum class WDMLighting(val value: String) {
    YES("Yes"),
    NO ("No"),
    BAD("Bad");

    companion object {
        val selectableValues = listOf(
            YES, NO, BAD
        )
    }
}
