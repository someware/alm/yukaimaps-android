package de.westnordost.streetcomplete.quests.wdm.flooring_material

import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.quests.wdm.flooring_material.WDMFlooringMaterial.ASPHALT
import de.westnordost.streetcomplete.quests.wdm.flooring_material.WDMFlooringMaterial.CONCRETE
import de.westnordost.streetcomplete.quests.wdm.flooring_material.WDMFlooringMaterial.WOOD
import de.westnordost.streetcomplete.quests.wdm.flooring_material.WDMFlooringMaterial.EARTH
import de.westnordost.streetcomplete.quests.wdm.flooring_material.WDMFlooringMaterial.GRAVEL
import de.westnordost.streetcomplete.quests.wdm.flooring_material.WDMFlooringMaterial.PAVINGSTONES
import de.westnordost.streetcomplete.quests.wdm.flooring_material.WDMFlooringMaterial.GRASS
import de.westnordost.streetcomplete.quests.wdm.flooring_material.WDMFlooringMaterial.CERAMICTILES
import de.westnordost.streetcomplete.quests.wdm.flooring_material.WDMFlooringMaterial.PLASTICMATTING
import de.westnordost.streetcomplete.quests.wdm.flooring_material.WDMFlooringMaterial.STEELPLATE
import de.westnordost.streetcomplete.quests.wdm.flooring_material.WDMFlooringMaterial.SAND
import de.westnordost.streetcomplete.quests.wdm.flooring_material.WDMFlooringMaterial.STONE
import de.westnordost.streetcomplete.quests.wdm.flooring_material.WDMFlooringMaterial.CARPET
import de.westnordost.streetcomplete.quests.wdm.flooring_material.WDMFlooringMaterial.RUBBER
import de.westnordost.streetcomplete.quests.wdm.flooring_material.WDMFlooringMaterial.CORK
import de.westnordost.streetcomplete.quests.wdm.flooring_material.WDMFlooringMaterial.FIBREGLASSGRATING
import de.westnordost.streetcomplete.quests.wdm.flooring_material.WDMFlooringMaterial.GLAZEDCERAMICTILES
import de.westnordost.streetcomplete.quests.wdm.flooring_material.WDMFlooringMaterial.VINYL
import de.westnordost.streetcomplete.quests.wdm.flooring_material.WDMFlooringMaterial.UNEVEN
import de.westnordost.streetcomplete.view.image_select.Item

fun WDMFlooringMaterial.asItem() = Item(this, iconResId, titleResId)

private val WDMFlooringMaterial.titleResId: Int get() = when (this) {
    ASPHALT ->  R.string.quest_wdm_flooring_material_asphalt
    CONCRETE -> R.string.quest_wdm_flooring_material_concrete
    WOOD ->      R.string.quest_wdm_flooring_material_wood
    EARTH ->      R.string.quest_wdm_flooring_material_earth
    GRAVEL ->      R.string.quest_wdm_flooring_material_gravel
    PAVINGSTONES ->      R.string.quest_wdm_flooring_material_paving_stones
    GRASS ->      R.string.quest_wdm_flooring_material_grass
    CERAMICTILES ->      R.string.quest_wdm_flooring_material_ceramic_tiles
    PLASTICMATTING ->      R.string.quest_wdm_flooring_material_plastic_matting
    STEELPLATE ->      R.string.quest_wdm_flooring_material_steel_plate
    SAND ->      R.string.quest_wdm_flooring_material_sand
    STONE ->      R.string.quest_wdm_flooring_material_stone
    CARPET ->      R.string.quest_wdm_flooring_material_carpet
    RUBBER ->      R.string.quest_wdm_flooring_material_rubber
    CORK ->      R.string.quest_wdm_flooring_material_cork
    FIBREGLASSGRATING ->      R.string.quest_wdm_flooring_material_fibreglass_grating
    GLAZEDCERAMICTILES ->      R.string.quest_wdm_flooring_material_glazed_ceramic_tiles
    VINYL ->      R.string.quest_wdm_flooring_material_vinyl
    UNEVEN ->      R.string.quest_wdm_flooring_material_uneven
}

private val WDMFlooringMaterial.iconResId: Int get() = when (this) {
    ASPHALT ->  R.drawable.wdm_flooring_material_asphalt
    CONCRETE -> R.drawable.wdm_flooring_material_concrete
    WOOD ->      R.drawable.wdm_flooring_material_wood
    EARTH ->      R.drawable.wdm_flooring_material_earth
    GRAVEL ->      R.drawable.wdm_flooring_material_gravel
    PAVINGSTONES ->      R.drawable.wdm_flooring_material_paving_stones
    GRASS ->      R.drawable.wdm_flooring_material_grass
    CERAMICTILES ->      R.drawable.wdm_flooring_material_ceramic_tiles
    PLASTICMATTING ->      R.drawable.wdm_default_photo
    STEELPLATE ->      R.drawable.wdm_flooring_material_steel_plate
    SAND ->      R.drawable.wdm_flooring_material_sand
    STONE ->      R.drawable.wdm_flooring_material_stone
    CARPET ->      R.drawable.wdm_flooring_material_carpet
    RUBBER ->      R.drawable.wdm_flooring_material_rubber
    CORK ->      R.drawable.wdm_flooring_material_cork
    FIBREGLASSGRATING ->      R.drawable.wdm_flooring_material_fibreglass_grating
    GLAZEDCERAMICTILES ->      R.drawable.wdm_flooring_material_glazed_ceramic_tiles
    VINYL ->      R.drawable.wdm_flooring_material_vinyl
    UNEVEN ->      R.drawable.wdm_flooring_material_uneven
}
