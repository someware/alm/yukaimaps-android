package de.westnordost.streetcomplete.quests.wdm.wheelchair_access

import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.data.osm.geometry.ElementGeometry
import de.westnordost.streetcomplete.data.osm.osmquests.OsmFilterQuestType
import de.westnordost.streetcomplete.data.user.achievements.EditTypeAchievement.WHEELCHAIR
import de.westnordost.streetcomplete.osm.Tags

class AddWDMWheelchairAccessOutsideLimitedNote : OsmFilterQuestType<String>() {

    override val elementFilter = """
        ways with SitePathLink !~ Stairs|Escalator
        and WheelchairAccess = limited
        and !WheelchairAccess:Description
    """
    override val changesetComment = "WDM Survey limited note wheelchair accessibility of outside places"
    override val wikiLink = null
    override val icon = R.drawable.ic_wdm_wheelchair
    override val achievements = listOf(WHEELCHAIR)


    override fun getTitle(tags: Map<String, String>) =
        if(tags["SitePathLink"] == "Pavement")
            R.string.quest_wdm_site_path_link_pavement_wheelchairAccess_outside_limited_note_title
        else if(tags["SitePathLink"] == "Footpath")
            R.string.quest_wdm_site_path_link_footpath_wheelchairAccess_outside_limited_note_title
        else if(tags["SitePathLink"] == "Street")
            R.string.quest_wdm_site_path_link_street_wheelchairAccess_outside_limited_note_title
        else if(tags["SitePathLink"] == "OpenSpace")
            R.string.quest_wdm_site_path_link_openspace_wheelchairAccess_outside_limited_note_title
        else if(tags["SitePathLink"] == "Concourse")
            R.string.quest_wdm_site_path_link_concourse_wheelchairAccess_outside_limited_note_title
        else if(tags["SitePathLink"] == "Passage")
            R.string.quest_wdm_site_path_link_passage_wheelchairAccess_outside_limited_note_title
        else if(tags["SitePathLink"] == "Ramp")
            R.string.quest_wdm_site_path_link_ramp_wheelchairAccess_outside_limited_note_title
        else if(tags["SitePathLink"] == "Crossing")
            R.string.quest_wdm_site_path_link_crossing_wheelchairAccess_outside_limited_note_title
        else if(tags["SitePathLink"] == "Quay")
            R.string.quest_wdm_site_path_link_quay_wheelchairAccess_outside_limited_note_title
        else if(tags["SitePathLink"] == "Hall")
            R.string.quest_wdm_site_path_link_hall_wheelchairAccess_outside_limited_note_title
        else if(tags["SitePathLink"] == "Elevator")
            R.string.quest_wdm_site_path_link_elevator_wheelchairAccess_outside_limited_note_title
        else
            R.string.quest_wdm_wheelchairAccess_outside_limited_note_title

    override fun createForm() = AddWDMWheelchairAccessOutsideLimitedNoteForm()

    override fun applyAnswerTo(answer: String, tags: Tags, geometry: ElementGeometry, timestampEdited: Long) {
        tags["WheelchairAccess:Description"] = answer
    }
}
