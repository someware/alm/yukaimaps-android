package de.westnordost.streetcomplete.quests.wdm.real_time_departures

import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.quests.AbstractOsmQuestForm
import de.westnordost.streetcomplete.quests.AnswerItem

class WDMRealTimeDeparturesForm : AbstractOsmQuestForm<Boolean>() {

    override val contentLayoutResId = R.layout.quest_wdm_real_time_departures

    override val buttonPanelAnswers = listOf(
        AnswerItem(R.string.quest_generic_hasFeature_no) { applyAnswer(false) },
        AnswerItem(R.string.quest_generic_hasFeature_yes) { applyAnswer(true) }
    )
}
