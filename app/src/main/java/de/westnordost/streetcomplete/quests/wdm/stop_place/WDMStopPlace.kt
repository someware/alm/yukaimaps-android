package de.westnordost.streetcomplete.quests.wdm.stop_place

enum class WDMStopPlace {
    RAILSTATION,
    METROSTATION,
    BUSSTATION,
    COACHSTATION,
    TRAMSTATION,
    FERRYPORT,
    LIFTSTATION,
    AIRPORT
}
