package de.westnordost.streetcomplete.quests.wdm.entrance_passage

import de.westnordost.streetcomplete.quests.AImageListQuestForm

class AddWDMEntrancePassageForm : AImageListQuestForm<WDMEntrancePassage, WDMEntrancePassage>() {

    override val items = WDMEntrancePassage.values().map { it.asItem() }
    override val itemsPerRow = 3

    override fun onClickOk(selectedItems: List<WDMEntrancePassage>) {
        applyAnswer(selectedItems.single())
    }
}
