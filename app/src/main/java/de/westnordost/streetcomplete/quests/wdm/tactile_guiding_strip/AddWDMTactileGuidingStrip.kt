package de.westnordost.streetcomplete.quests.wdm.tactile_guiding_strip

import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.data.osm.geometry.ElementGeometry
import de.westnordost.streetcomplete.data.osm.osmquests.OsmFilterQuestType
import de.westnordost.streetcomplete.data.user.achievements.EditTypeAchievement
import de.westnordost.streetcomplete.osm.Tags
import de.westnordost.streetcomplete.osm.updateWithCheckDate
import de.westnordost.streetcomplete.util.ktx.toYesNo

class AddWDMTactileGuidingStrip : OsmFilterQuestType<Boolean>() {

    override val elementFilter = """
        ways with SitePathLink
        and SitePathLink !~ Stairs|Escalator|Travelator
        and !TactileGuidingStrip
    """

    override val changesetComment = "WDM Survey tactile guiding strip"
    override val wikiLink = null
    override val icon = R.drawable.ic_wdm_tactile_guiding_strip
    override val achievements = listOf(EditTypeAchievement.WHEELCHAIR)

    override fun getTitle(tags: Map<String, String>) = R.string.quest_wdm_tactile_guiding_strip_title

    override fun createForm() = WDMTactileGuidingStripForm()

    override fun applyAnswerTo(answer: Boolean, tags: Tags, geometry: ElementGeometry, timestampEdited: Long) {
        tags.updateWithCheckDate("TactileGuidingStrip", answer.toYesNo())
    }
}
