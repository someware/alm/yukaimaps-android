package de.westnordost.streetcomplete.quests.wdm.visual_guidance_bands

import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.data.osm.geometry.ElementGeometry
import de.westnordost.streetcomplete.data.osm.osmquests.OsmFilterQuestType
import de.westnordost.streetcomplete.data.quest.AllCountriesExcept
import de.westnordost.streetcomplete.data.user.achievements.EditTypeAchievement
import de.westnordost.streetcomplete.data.user.achievements.EditTypeAchievement.CITIZEN
import de.westnordost.streetcomplete.osm.Tags
import de.westnordost.streetcomplete.quests.YesNoQuestForm
import de.westnordost.streetcomplete.util.ktx.toYesNo

class AddWDMVisualGuidanceBands : OsmFilterQuestType<Boolean>() {

    override val elementFilter = """
        ways with SitePathLink = Crossing
         and !VisualGuidanceBands
    """

    override val changesetComment = "Specify visual guidance bands"
    override val wikiLink = null
    override val icon = R.drawable.ic_quest_wdm_visual_guidance_bands
    override val achievements = listOf(EditTypeAchievement.WHEELCHAIR)

    override fun getTitle(tags: Map<String, String>) = R.string.quest_wdm_visual_guidance_bands_title

    override fun createForm() = YesNoQuestForm()

    override fun applyAnswerTo(answer: Boolean, tags: Tags, geometry: ElementGeometry, timestampEdited: Long) {
        tags["VisualGuidanceBands"] = answer.toYesNo()
    }
}
