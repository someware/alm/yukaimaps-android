package de.westnordost.streetcomplete.quests.wdm.ticket

import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.data.elementfilter.toElementFilterExpression
import de.westnordost.streetcomplete.data.osm.geometry.ElementGeometry
import de.westnordost.streetcomplete.data.osm.mapdata.Element
import de.westnordost.streetcomplete.data.osm.mapdata.MapDataWithGeometry
import de.westnordost.streetcomplete.data.osm.osmquests.OsmElementQuestType
import de.westnordost.streetcomplete.data.user.achievements.EditTypeAchievement.WHEELCHAIR
import de.westnordost.streetcomplete.osm.Tags

class AddWDMTickets : OsmElementQuestType<WDMTicketsAnswer> {

    private val filter by lazy { """
        nodes, ways with PointOfInterest = StopPlace
    """.toElementFilterExpression() }

    override val changesetComment = "Specify what kind of ticket"
    override val wikiLink = null
    override val icon = R.drawable.ic_quest_wdm_tickets
    override val isDeleteElementEnabled = true
    override val achievements = listOf(WHEELCHAIR)

    override fun getTitle(tags: Map<String, String>) = R.string.quest_wdm_ticket_title

    override fun getApplicableElements(mapData: MapDataWithGeometry): Iterable<Element> =
        mapData.nodes.filter { isApplicableTo(it) }

    override fun isApplicableTo(element: Element): Boolean =
        /* Only recycling containers that do either not have any recycling:* tag yet or
         * haven't been touched for 2 years and are exclusively recycling types selectable in
         * StreetComplete. */
        filter.matches(element) &&
            !element.hasAnyTickets()

    override fun createForm() = AddWDMTicketsForm()

    override fun applyAnswerTo(answer: WDMTicketsAnswer, tags: Tags, geometry: ElementGeometry, timestampEdited: Long) {
        if (answer is WDMTickets) {
            applyWDMTicketAnswer(answer.tickets, tags)
        }
    }

    private fun applyWDMTicketAnswer(tickets: List<WDMTicket>, tags: Tags) {
        // set selected Tickets:* taggings to "yes"
        val selectedTickets = tickets.map { "Tickets:${it.value}" }
        for (ticket in selectedTickets) {
            tags[ticket] = "yes"
        }
    }

    private fun Element.hasAnyTickets(): Boolean =
        tags.any { it.key.startsWith("Tickets:") && it.value == "yes" }
}
