package de.westnordost.streetcomplete.quests.wdm.baby_change

import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.data.elementfilter.toElementFilterExpression
import de.westnordost.streetcomplete.data.osm.geometry.ElementGeometry
import de.westnordost.streetcomplete.data.osm.mapdata.Element
import de.westnordost.streetcomplete.data.osm.mapdata.MapDataWithGeometry
import de.westnordost.streetcomplete.data.osm.osmquests.OsmElementQuestType
import de.westnordost.streetcomplete.data.user.achievements.EditTypeAchievement.WHEELCHAIR
import de.westnordost.streetcomplete.osm.Tags
import de.westnordost.streetcomplete.util.ktx.toYesNo

class AddWDMBabyChange : OsmElementQuestType<WDMBabyChangesAnswer> {

    private val filter by lazy { """
        nodes, ways with PointOfInterest = StopPlace
        and Toilets ~ Yes|Bad
        and !Toilets:BabyChange
    """.toElementFilterExpression() }

    override val changesetComment = "Specify what kind of toilet baby change"
    override val wikiLink = null
    override val icon = R.drawable.ic_quest_wdm_toilets_baby_change
    override val achievements = listOf(WHEELCHAIR)

    override fun getTitle(tags: Map<String, String>) = R.string.quest_wdm_baby_change_title

    override fun getApplicableElements(mapData: MapDataWithGeometry): Iterable<Element> =
        mapData.nodes.filter { isApplicableTo(it) }

    override fun isApplicableTo(element: Element): Boolean =
        /* Only recycling containers that do either not have any recycling:* tag yet or
         * haven't been touched for 2 years and are exclusively recycling types selectable in
         * StreetComplete. */
        filter.matches(element)

    override fun createForm() = AddWDMBabyChangeForm()

    override fun applyAnswerTo(answer: WDMBabyChangesAnswer, tags: Tags, geometry: ElementGeometry, timestampEdited: Long) {
        if (answer is WDMBabyChanges) {
            applyWDMBabyChangeAnswer(answer.babyChanges, tags)
        }
    }

    private fun applyWDMBabyChangeAnswer(babyChanges: List<WDMBabyChange>, tags: Tags) {
        val selectedBabyChanges = babyChanges.map { "${it.value}" }
        for (babyChange in selectedBabyChanges) {
            tags["Toilets:BabyChange"] = babyChange;
        }
    }
}
