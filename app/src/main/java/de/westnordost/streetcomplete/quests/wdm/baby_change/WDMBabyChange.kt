package de.westnordost.streetcomplete.quests.wdm.baby_change

/** All assistance:* keys known to StreetComplete */
enum class WDMBabyChange(val value: String) {
    PMR("Yes"),
    NOPMR("Bad"),
    NONE("No");

    companion object {
        val selectableValues = listOf(
            PMR, NOPMR, NONE
        )
    }
}
