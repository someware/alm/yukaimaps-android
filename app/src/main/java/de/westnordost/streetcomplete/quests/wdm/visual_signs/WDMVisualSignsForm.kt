package de.westnordost.streetcomplete.quests.wdm.visual_signs

import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.quests.AbstractOsmQuestForm
import de.westnordost.streetcomplete.quests.AnswerItem
import de.westnordost.streetcomplete.quests.wdm.visual_signs.WDMVisualSigns.LIMITED
import de.westnordost.streetcomplete.quests.wdm.visual_signs.WDMVisualSigns.NO
import de.westnordost.streetcomplete.quests.wdm.visual_signs.WDMVisualSigns.YES

open class WDMVisualSignsForm : AbstractOsmQuestForm<WDMVisualSigns>() {

    override val buttonPanelAnswers = listOf(
        AnswerItem(R.string.quest_generic_hasFeature_no) { applyAnswer(NO) },
        AnswerItem(R.string.quest_generic_hasFeature_yes) { applyAnswer(YES) },
        AnswerItem(R.string.quest_wheelchairAccess_limited) {
            applyAnswer(LIMITED)
        },
    )
}
