package de.westnordost.streetcomplete.quests.wdm.slope

import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.data.osm.geometry.ElementGeometry
import de.westnordost.streetcomplete.data.osm.osmquests.OsmFilterQuestType
import de.westnordost.streetcomplete.data.user.achievements.EditTypeAchievement.WHEELCHAIR
import de.westnordost.streetcomplete.osm.Tags

class AddWDMSlope : OsmFilterQuestType<Int>() {

    override val elementFilter = """
        ways with SitePathLink
        and SitePathLink !~ Stairs|Escalator
        and !Slope
    """

    override val changesetComment = "WDM Specify slope"
    override val wikiLink = null
    override val icon = R.drawable.ic_quest_wdm_slope
    override val achievements = listOf(WHEELCHAIR)

    override fun getTitle(tags: Map<String, String>) = R.string.quest_wdm_slope_title

    override fun createForm() = AddWDMSlopeForm()

    override fun applyAnswerTo(answer: Int, tags: Tags, geometry: ElementGeometry, timestampEdited: Long) {
        tags["Slope"] = answer.toString()
    }
}
