package de.westnordost.streetcomplete.quests.wdm.parking_visibility

enum class WDMParkingVisibility {
    UNMARKED,
    SIGNAGEONLY,
    MARKINGONLY,
    DOCKS,
    DEMARCATED
}
