package de.westnordost.streetcomplete.quests.wdm.automatic_door

import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.data.osm.geometry.ElementGeometry
import de.westnordost.streetcomplete.data.osm.osmquests.OsmFilterQuestType
import de.westnordost.streetcomplete.data.quest.AllCountriesExcept
import de.westnordost.streetcomplete.data.user.achievements.EditTypeAchievement
import de.westnordost.streetcomplete.data.user.achievements.EditTypeAchievement.CITIZEN
import de.westnordost.streetcomplete.osm.Tags
import de.westnordost.streetcomplete.quests.YesNoQuestForm
import de.westnordost.streetcomplete.util.ktx.toYesNo

class AddWDMAudioAnnouncements : OsmFilterQuestType<Boolean>() {

    override val elementFilter = """
        nodes, ways with SitePathLink = Elevator
         and !AudioAnnouncements
    """

    override val changesetComment = "Specify audio announcements"
    override val wikiLink = null
    override val icon = R.drawable.ic_quest_wdm_audio_announcements
    override val achievements = listOf(EditTypeAchievement.WHEELCHAIR)

    override fun getTitle(tags: Map<String, String>) = R.string.quest_wdm_audio_announcements_title

    override fun createForm() = YesNoQuestForm()

    override fun applyAnswerTo(answer: Boolean, tags: Tags, geometry: ElementGeometry, timestampEdited: Long) {
        tags["AudioAnnouncements"] = answer.toYesNo()
    }
}
