package de.westnordost.streetcomplete.quests.wdm.highway_type

/** All HighwayType:* keys known to StreetComplete */
enum class WDMHighwayType(val value: String) {
    STREET("Street"),
    LIMITEDSPEEDSTREET ("LimitedSpeedStreet"),
    LIVINGSTREET("LivingStreet"),
    PEDESTRIAN("Pedestrian"),
    BICYCLE("Bicycle"),
    UNCLASSIFIED("Unclassified");

    companion object {
        val selectableValues = listOf(
            STREET, LIMITEDSPEEDSTREET, LIVINGSTREET, PEDESTRIAN, BICYCLE, UNCLASSIFIED
        )
    }
}
