package de.westnordost.streetcomplete.quests.wdm.flooring_material

import de.westnordost.streetcomplete.quests.AImageListQuestForm

class AddWDMFlooringMaterialParkingBayForm : AImageListQuestForm<WDMFlooringMaterial, WDMFlooringMaterial>() {

    override val items = WDMFlooringMaterial.values().map { it.asItem() }
    override val itemsPerRow = 3

    override fun onClickOk(selectedItems: List<WDMFlooringMaterial>) {
        applyAnswer(selectedItems.single())
    }
}
