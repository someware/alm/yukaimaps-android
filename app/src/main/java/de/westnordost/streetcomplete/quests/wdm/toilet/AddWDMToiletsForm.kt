package de.westnordost.streetcomplete.quests.wdm.toilet

import android.os.Bundle
import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.quests.AImageListQuestForm

class AddWDMToiletsForm :
    AImageListQuestForm<List<WDMToilet>, WDMToiletsAnswer>() {

    override val descriptionResId = R.string.quest_wdm_toilets_note

    override val items get() = WDMToilet.selectableValues.map { it.asItem() }

    override val maxSelectableItems = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        imageSelector.cellLayoutId = R.layout.cell_icon_select_with_label_below
    }

    override fun onClickOk(selectedItems: List<List<WDMToilet>>) {
        applyAnswer(WDMToilets(selectedItems.flatten()))
    }
}
