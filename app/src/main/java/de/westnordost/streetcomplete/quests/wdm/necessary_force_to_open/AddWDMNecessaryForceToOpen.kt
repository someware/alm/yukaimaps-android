package de.westnordost.streetcomplete.quests.wdm.necessary_force_to_open

import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.data.osm.geometry.ElementGeometry
import de.westnordost.streetcomplete.data.osm.osmquests.OsmFilterQuestType
import de.westnordost.streetcomplete.data.user.achievements.EditTypeAchievement.WHEELCHAIR
import de.westnordost.streetcomplete.osm.Tags

class AddWDMNecessaryForceToOpen : OsmFilterQuestType<Int>() {

    override val elementFilter = """
        nodes with Entrance
        and EntrancePassage ~ Door|Gate|Barrier
        and !NecessaryForceToOpen
    """

    override val changesetComment = "WDM Specify necessary force to open"
    override val wikiLink = null
    override val icon = R.drawable.ic_quest_wdm_necessary_force_to_open
    override val achievements = listOf(WHEELCHAIR)

    override fun getTitle(tags: Map<String, String>) = R.string.quest_wdm_necessary_force_to_open_title

    override fun createForm() = AddWDMNecessaryForceToOpenForm()

    override fun applyAnswerTo(answer: Int, tags: Tags, geometry: ElementGeometry, timestampEdited: Long) {
        tags["NecessaryForceToOpen"] = answer.toString()
    }
}
