package de.westnordost.streetcomplete.quests.wdm.highway_type

import android.os.Bundle
import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.quests.AImageListQuestForm

class AddWDMHighwayTypesForm :
    AImageListQuestForm<List<WDMHighwayType>, WDMHighwayTypesAnswer>() {

    override val descriptionResId = R.string.quest_wdm_highway_type_note

    override val items get() = WDMHighwayType.selectableValues.map { it.asItem() }

    override val maxSelectableItems = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        imageSelector.cellLayoutId = R.layout.cell_icon_select_with_label_below
    }

    override fun onClickOk(selectedItems: List<List<WDMHighwayType>>) {
        applyAnswer(WDMHighwayTypes(selectedItems.flatten()))
    }
}
