package de.westnordost.streetcomplete.quests.wdm.entrance_passage

import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.quests.wdm.entrance_passage.WDMEntrancePassage.OPENING
import de.westnordost.streetcomplete.quests.wdm.entrance_passage.WDMEntrancePassage.DOOR
import de.westnordost.streetcomplete.quests.wdm.entrance_passage.WDMEntrancePassage.GATE
import de.westnordost.streetcomplete.quests.wdm.entrance_passage.WDMEntrancePassage.BARRIER
import de.westnordost.streetcomplete.view.image_select.Item

fun WDMEntrancePassage.asItem() = Item(this, iconResId, titleResId)

private val WDMEntrancePassage.titleResId: Int get() = when (this) {
    OPENING ->  R.string.quest_wdm_entrance_passage_opening
    DOOR -> R.string.quest_wdm_entrance_passage_door
    GATE ->      R.string.quest_wdm_entrance_passage_gate
    BARRIER ->      R.string.quest_wdm_entrance_passage_barrier
}

private val WDMEntrancePassage.iconResId: Int get() = when (this) {
    OPENING ->  R.drawable.wdm_default_photo
    DOOR -> R.drawable.wdm_default_photo
    GATE ->      R.drawable.wdm_default_photo
    BARRIER ->      R.drawable.wdm_default_photo
}
