package de.westnordost.streetcomplete.quests.wdm.entrance_passage

import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.data.elementfilter.toElementFilterExpression
import de.westnordost.streetcomplete.data.osm.geometry.ElementGeometry
import de.westnordost.streetcomplete.data.osm.mapdata.Element
import de.westnordost.streetcomplete.data.osm.mapdata.MapDataWithGeometry
import de.westnordost.streetcomplete.data.osm.mapdata.filter
import de.westnordost.streetcomplete.data.osm.osmquests.OsmFilterQuestType
import de.westnordost.streetcomplete.data.user.achievements.EditTypeAchievement.CITIZEN
import de.westnordost.streetcomplete.data.user.achievements.EditTypeAchievement.WHEELCHAIR
import de.westnordost.streetcomplete.osm.Tags
import de.westnordost.streetcomplete.quests.wdm.entrance_passage.WDMEntrancePassage.OPENING
import de.westnordost.streetcomplete.quests.wdm.entrance_passage.WDMEntrancePassage.DOOR
import de.westnordost.streetcomplete.quests.wdm.entrance_passage.WDMEntrancePassage.GATE
import de.westnordost.streetcomplete.quests.wdm.entrance_passage.WDMEntrancePassage.BARRIER

class AddWDMEntrancePassage : OsmFilterQuestType<WDMEntrancePassage>() {

    override val elementFilter = "nodes with Entrance and !EntrancePassage"
    override val changesetComment = "Specify type of entrance passage"
    override val wikiLink = null
    override val icon = R.drawable.ic_quest_wdm_entrance_passage
    override val achievements = listOf(WHEELCHAIR)

    override fun getTitle(tags: Map<String, String>) = R.string.quest_wdm_entrance_passage_title

    override fun createForm() = AddWDMEntrancePassageForm()

    override fun applyAnswerTo(answer: WDMEntrancePassage, tags: Tags, geometry: ElementGeometry, timestampEdited: Long) {
        when (answer) {
            OPENING -> {
                tags["EntrancePassage"] = "Opening"
            }
            DOOR -> {
                tags["EntrancePassage"] = "Door"
            }
            GATE -> {
                tags["EntrancePassage"] = "Gate"
            }
            BARRIER -> {
                tags["EntrancePassage"] = "Barrier"
            }
        }
    }
}
