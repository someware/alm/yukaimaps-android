package de.westnordost.streetcomplete.quests.wdm.incline

import de.westnordost.streetcomplete.osm.Tags

enum class WDMIncline {
    UP, DOWN/*, NO*/
}

fun WDMIncline.applyTo(tags: Tags) {
    tags["Incline"] = when (this) {
        WDMIncline.UP -> "Up"
        WDMIncline.DOWN -> "Down"
        //WDMIncline.NO -> "No"
    }
}
