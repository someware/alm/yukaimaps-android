package de.westnordost.streetcomplete.quests.wdm.ticket

sealed interface WDMTicketsAnswer

data class WDMTickets(val tickets: List<WDMTicket>) : WDMTicketsAnswer
