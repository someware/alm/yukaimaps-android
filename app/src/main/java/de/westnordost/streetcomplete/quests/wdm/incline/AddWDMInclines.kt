package de.westnordost.streetcomplete.quests.wdm.incline

import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.data.elementfilter.toElementFilterExpression
import de.westnordost.streetcomplete.data.osm.geometry.ElementGeometry
import de.westnordost.streetcomplete.data.osm.mapdata.Element
import de.westnordost.streetcomplete.data.osm.mapdata.MapDataWithGeometry
import de.westnordost.streetcomplete.data.osm.osmquests.OsmElementQuestType
import de.westnordost.streetcomplete.data.osm.osmquests.OsmFilterQuestType
import de.westnordost.streetcomplete.data.user.achievements.EditTypeAchievement.WHEELCHAIR
import de.westnordost.streetcomplete.osm.Tags
import de.westnordost.streetcomplete.quests.incline_direction.AddInclineForm
import de.westnordost.streetcomplete.quests.incline_direction.Incline
import de.westnordost.streetcomplete.quests.incline_direction.applyTo

class AddWDMInclines : OsmFilterQuestType<WDMIncline>() {

    override val elementFilter = """
        ways with SitePathLink
        and SitePathLink !~ Stairs|Escalator
        and Slope
        and Slope != 0
        and !Incline
    """

    override val changesetComment = "Specify what kind of incline"
    override val wikiLink = null
    override val icon = R.drawable.ic_quest_wdm_incline
    override val achievements = listOf(WHEELCHAIR)

    override fun getTitle(tags: Map<String, String>) = R.string.quest_wdm_incline_title

    override fun createForm() = AddWDMInclinesForm()

    override fun applyAnswerTo(answer: WDMIncline, tags: Tags, geometry: ElementGeometry, timestampEdited: Long) =
        answer.applyTo(tags)
}
