package de.westnordost.streetcomplete.quests.wdm.audible_signals

enum class WDMAudibleSignals(val osmValue: String) {
    YES("yes"),
    LIMITED("limited"),
    NO("no"),
}
