package de.westnordost.streetcomplete.quests.wdm.parking_visibility

import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.quests.wdm.parking_visibility.WDMParkingVisibility.UNMARKED
import de.westnordost.streetcomplete.quests.wdm.parking_visibility.WDMParkingVisibility.SIGNAGEONLY
import de.westnordost.streetcomplete.quests.wdm.parking_visibility.WDMParkingVisibility.MARKINGONLY
import de.westnordost.streetcomplete.quests.wdm.parking_visibility.WDMParkingVisibility.DOCKS
import de.westnordost.streetcomplete.quests.wdm.parking_visibility.WDMParkingVisibility.DEMARCATED
import de.westnordost.streetcomplete.view.image_select.Item

fun WDMParkingVisibility.asItem() = Item(this, iconResId, titleResId)

private val WDMParkingVisibility.titleResId: Int get() = when (this) {
    UNMARKED ->  R.string.quest_wdm_parking_visibility_unmarked
    SIGNAGEONLY -> R.string.quest_wdm_parking_visibility_signage_only
    MARKINGONLY ->      R.string.quest_wdm_parking_visibility_marking_only
    DOCKS ->      R.string.quest_wdm_parking_visibility_docks
    DEMARCATED ->      R.string.quest_wdm_parking_visibility_demarcated
}

private val WDMParkingVisibility.iconResId: Int get() = when (this) {
    UNMARKED ->  R.drawable.wdm_parking_visibility_unmarked
    SIGNAGEONLY -> R.drawable.wdm_default_photo
    MARKINGONLY ->      R.drawable.wdm_parking_visibility_marking_only
    DOCKS ->      R.drawable.wdm_parking_visibility_docks
    DEMARCATED ->      R.drawable.wdm_parking_visibility_demarcated
}
