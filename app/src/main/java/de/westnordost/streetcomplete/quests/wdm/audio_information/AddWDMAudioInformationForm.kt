package de.westnordost.streetcomplete.quests.wdm.audio_information

import de.westnordost.streetcomplete.R

class AddWDMAudioInformationForm : WDMAudioInformationForm() {
    override val contentLayoutResId = R.layout.quest_wdm_audio_information_explanation
}
