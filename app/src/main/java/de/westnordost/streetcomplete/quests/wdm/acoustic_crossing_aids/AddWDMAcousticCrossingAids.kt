package de.westnordost.streetcomplete.quests.wdm.acoustic_crossing_aids

import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.data.elementfilter.toElementFilterExpression
import de.westnordost.streetcomplete.data.osm.geometry.ElementGeometry
import de.westnordost.streetcomplete.data.osm.mapdata.Element
import de.westnordost.streetcomplete.data.osm.mapdata.MapDataWithGeometry
import de.westnordost.streetcomplete.data.osm.mapdata.filter
import de.westnordost.streetcomplete.data.osm.osmquests.OsmFilterQuestType
import de.westnordost.streetcomplete.data.user.achievements.EditTypeAchievement.CITIZEN
import de.westnordost.streetcomplete.data.user.achievements.EditTypeAchievement.WHEELCHAIR
import de.westnordost.streetcomplete.osm.Tags
import de.westnordost.streetcomplete.quests.wdm.acoustic_crossing_aids.WDMAcousticCrossingAids.NONE
import de.westnordost.streetcomplete.quests.wdm.acoustic_crossing_aids.WDMAcousticCrossingAids.GOOD
import de.westnordost.streetcomplete.quests.wdm.acoustic_crossing_aids.WDMAcousticCrossingAids.WORN
import de.westnordost.streetcomplete.quests.wdm.acoustic_crossing_aids.WDMAcousticCrossingAids.DISCOMFORTABLE
import de.westnordost.streetcomplete.quests.wdm.acoustic_crossing_aids.WDMAcousticCrossingAids.HAZARDOUS

class AddWDMAcousticCrossingAids : OsmFilterQuestType<WDMAcousticCrossingAids>() {

    override val elementFilter = "ways with SitePathLink and SitePathLink = Crossing and !AcousticCrossingAids"
    override val changesetComment = "Specify type of acoustic crossing aids"
    override val wikiLink = null
    override val icon = R.drawable.ic_quest_wdm_acoustic_crossing_aids
    override val achievements = listOf(WHEELCHAIR)

    override fun getTitle(tags: Map<String, String>) = R.string.quest_wdm_acoustic_crossing_aids_title

    override fun createForm() = AddWDMAcousticCrossingAidsForm()

    override fun applyAnswerTo(answer: WDMAcousticCrossingAids, tags: Tags, geometry: ElementGeometry, timestampEdited: Long) {
        when (answer) {
            NONE -> {
                tags["AcousticCrossingAids"] = "None"
            }
            GOOD -> {
                tags["AcousticCrossingAids"] = "Good"
            }
            WORN -> {
                tags["AcousticCrossingAids"] = "Worn"
            }
            DISCOMFORTABLE -> {
                tags["AcousticCrossingAids"] = "Discomfortable"
            }
            HAZARDOUS -> {
                tags["AcousticCrossingAids"] = "Hazardous"
            }
        }
    }
}
