package de.westnordost.streetcomplete.quests

import de.westnordost.countryboundaries.CountryBoundaries
import de.westnordost.osmfeatures.FeatureDictionary
import de.westnordost.streetcomplete.data.meta.CountryInfos
import de.westnordost.streetcomplete.data.osmnotes.notequests.OsmNoteQuestType
import de.westnordost.streetcomplete.data.quest.QuestTypeRegistry
import de.westnordost.streetcomplete.quests.accepts_cards.AddAcceptsCards
import de.westnordost.streetcomplete.quests.accepts_cash.AddAcceptsCash
import de.westnordost.streetcomplete.quests.access_point_ref.AddAccessPointRef
import de.westnordost.streetcomplete.quests.address.AddAddressStreet
import de.westnordost.streetcomplete.quests.address.AddHousenumber
import de.westnordost.streetcomplete.quests.air_conditioning.AddAirConditioning
import de.westnordost.streetcomplete.quests.air_pump.AddAirCompressor
import de.westnordost.streetcomplete.quests.air_pump.AddBicyclePump
import de.westnordost.streetcomplete.quests.amenity_cover.AddAmenityCover
import de.westnordost.streetcomplete.quests.atm_cashin.AddAtmCashIn
import de.westnordost.streetcomplete.quests.atm_operator.AddAtmOperator
import de.westnordost.streetcomplete.quests.baby_changing_table.AddBabyChangingTable
import de.westnordost.streetcomplete.quests.barrier_bicycle_barrier_installation.AddBicycleBarrierInstallation
import de.westnordost.streetcomplete.quests.barrier_bicycle_barrier_type.AddBicycleBarrierType
import de.westnordost.streetcomplete.quests.barrier_type.AddBarrierOnPath
import de.westnordost.streetcomplete.quests.barrier_type.AddBarrierOnRoad
import de.westnordost.streetcomplete.quests.barrier_type.AddBarrierType
import de.westnordost.streetcomplete.quests.barrier_type.AddStileType
import de.westnordost.streetcomplete.quests.bench_backrest.AddBenchBackrest
import de.westnordost.streetcomplete.quests.bike_parking_capacity.AddBikeParkingCapacity
import de.westnordost.streetcomplete.quests.bike_parking_cover.AddBikeParkingCover
import de.westnordost.streetcomplete.quests.bike_parking_type.AddBikeParkingType
import de.westnordost.streetcomplete.quests.bike_rental_capacity.AddBikeRentalCapacity
import de.westnordost.streetcomplete.quests.bike_rental_type.AddBikeRentalType
import de.westnordost.streetcomplete.quests.bike_shop.AddBikeRepairAvailability
import de.westnordost.streetcomplete.quests.bike_shop.AddSecondHandBicycleAvailability
import de.westnordost.streetcomplete.quests.board_type.AddBoardType
import de.westnordost.streetcomplete.quests.bollard_type.AddBollardType
import de.westnordost.streetcomplete.quests.bridge_structure.AddBridgeStructure
import de.westnordost.streetcomplete.quests.building_entrance.AddEntrance
import de.westnordost.streetcomplete.quests.building_entrance_reference.AddEntranceReference
import de.westnordost.streetcomplete.quests.building_levels.AddBuildingLevels
import de.westnordost.streetcomplete.quests.building_type.AddBuildingType
import de.westnordost.streetcomplete.quests.building_underground.AddIsBuildingUnderground
import de.westnordost.streetcomplete.quests.bus_stop_bench.AddBenchStatusOnBusStop
import de.westnordost.streetcomplete.quests.bus_stop_bin.AddBinStatusOnBusStop
import de.westnordost.streetcomplete.quests.bus_stop_lit.AddBusStopLit
import de.westnordost.streetcomplete.quests.bus_stop_name.AddBusStopName
import de.westnordost.streetcomplete.quests.bus_stop_ref.AddBusStopRef
import de.westnordost.streetcomplete.quests.bus_stop_shelter.AddBusStopShelter
import de.westnordost.streetcomplete.quests.camera_type.AddCameraType
import de.westnordost.streetcomplete.quests.camping.AddCampDrinkingWater
import de.westnordost.streetcomplete.quests.camping.AddCampPower
import de.westnordost.streetcomplete.quests.camping.AddCampShower
import de.westnordost.streetcomplete.quests.camping.AddCampType
import de.westnordost.streetcomplete.quests.car_wash_type.AddCarWashType
import de.westnordost.streetcomplete.quests.charging_station_capacity.AddChargingStationCapacity
import de.westnordost.streetcomplete.quests.charging_station_operator.AddChargingStationOperator
import de.westnordost.streetcomplete.quests.clothing_bin_operator.AddClothingBinOperator
import de.westnordost.streetcomplete.quests.construction.MarkCompletedBuildingConstruction
import de.westnordost.streetcomplete.quests.construction.MarkCompletedHighwayConstruction
import de.westnordost.streetcomplete.quests.crossing.AddCrossing
import de.westnordost.streetcomplete.quests.crossing_island.AddCrossingIsland
import de.westnordost.streetcomplete.quests.crossing_type.AddCrossingType
import de.westnordost.streetcomplete.quests.cycleway.AddCycleway
import de.westnordost.streetcomplete.quests.defibrillator.AddIsDefibrillatorIndoor
import de.westnordost.streetcomplete.quests.diet_type.AddHalal
import de.westnordost.streetcomplete.quests.diet_type.AddKosher
import de.westnordost.streetcomplete.quests.diet_type.AddVegan
import de.westnordost.streetcomplete.quests.diet_type.AddVegetarian
import de.westnordost.streetcomplete.quests.drinking_water.AddDrinkingWater
import de.westnordost.streetcomplete.quests.drinking_water_type.AddDrinkingWaterType
import de.westnordost.streetcomplete.quests.existence.CheckExistence
import de.westnordost.streetcomplete.quests.ferry.AddFerryAccessMotorVehicle
import de.westnordost.streetcomplete.quests.ferry.AddFerryAccessPedestrian
import de.westnordost.streetcomplete.quests.fire_hydrant.AddFireHydrantType
import de.westnordost.streetcomplete.quests.fire_hydrant_diameter.AddFireHydrantDiameter
import de.westnordost.streetcomplete.quests.fire_hydrant_position.AddFireHydrantPosition
import de.westnordost.streetcomplete.quests.fire_hydrant_ref.AddFireHydrantRef
import de.westnordost.streetcomplete.quests.foot.AddProhibitedForPedestrians
import de.westnordost.streetcomplete.quests.fuel_service.AddFuelSelfService
import de.westnordost.streetcomplete.quests.general_fee.AddGeneralFee
import de.westnordost.streetcomplete.quests.grit_bin_seasonal.AddGritBinSeasonal
import de.westnordost.streetcomplete.quests.hairdresser.AddHairdresserCustomers
import de.westnordost.streetcomplete.quests.handrail.AddHandrail
import de.westnordost.streetcomplete.quests.incline_direction.AddBicycleIncline
import de.westnordost.streetcomplete.quests.incline_direction.AddStepsIncline
import de.westnordost.streetcomplete.quests.internet_access.AddInternetAccess
import de.westnordost.streetcomplete.quests.kerb_height.AddKerbHeight
import de.westnordost.streetcomplete.quests.lanes.AddLanes
import de.westnordost.streetcomplete.quests.leaf_detail.AddForestLeafType
import de.westnordost.streetcomplete.quests.level.AddLevel
import de.westnordost.streetcomplete.quests.max_height.AddMaxHeight
import de.westnordost.streetcomplete.quests.max_height.AddMaxPhysicalHeight
import de.westnordost.streetcomplete.quests.max_speed.AddMaxSpeed
import de.westnordost.streetcomplete.quests.max_weight.AddMaxWeight
import de.westnordost.streetcomplete.quests.memorial_type.AddMemorialType
import de.westnordost.streetcomplete.quests.motorcycle_parking_capacity.AddMotorcycleParkingCapacity
import de.westnordost.streetcomplete.quests.motorcycle_parking_cover.AddMotorcycleParkingCover
import de.westnordost.streetcomplete.quests.oneway.AddOneway
import de.westnordost.streetcomplete.quests.oneway_suspects.AddSuspectedOneway
import de.westnordost.streetcomplete.quests.oneway_suspects.data.TrafficFlowSegmentsApi
import de.westnordost.streetcomplete.quests.oneway_suspects.data.WayTrafficFlowDao
import de.westnordost.streetcomplete.quests.opening_hours.AddOpeningHours
import de.westnordost.streetcomplete.quests.opening_hours_signed.CheckOpeningHoursSigned
import de.westnordost.streetcomplete.quests.orchard_produce.AddOrchardProduce
import de.westnordost.streetcomplete.quests.parking_access.AddBikeParkingAccess
import de.westnordost.streetcomplete.quests.parking_access.AddParkingAccess
import de.westnordost.streetcomplete.quests.parking_fee.AddBikeParkingFee
import de.westnordost.streetcomplete.quests.parking_fee.AddParkingFee
import de.westnordost.streetcomplete.quests.parking_type.AddParkingType
import de.westnordost.streetcomplete.quests.pitch_lit.AddPitchLit
import de.westnordost.streetcomplete.quests.place_name.AddPlaceName
import de.westnordost.streetcomplete.quests.playground_access.AddPlaygroundAccess
import de.westnordost.streetcomplete.quests.police_type.AddPoliceType
import de.westnordost.streetcomplete.quests.postbox_collection_times.AddPostboxCollectionTimes
import de.westnordost.streetcomplete.quests.postbox_ref.AddPostboxRef
import de.westnordost.streetcomplete.quests.postbox_royal_cypher.AddPostboxRoyalCypher
import de.westnordost.streetcomplete.quests.powerpoles_material.AddPowerPolesMaterial
import de.westnordost.streetcomplete.quests.railway_crossing.AddRailwayCrossingBarrier
import de.westnordost.streetcomplete.quests.recycling.AddRecyclingType
import de.westnordost.streetcomplete.quests.recycling_glass.DetermineRecyclingGlass
import de.westnordost.streetcomplete.quests.recycling_material.AddRecyclingContainerMaterials
import de.westnordost.streetcomplete.quests.religion.AddReligionToPlaceOfWorship
import de.westnordost.streetcomplete.quests.religion.AddReligionToWaysideShrine
import de.westnordost.streetcomplete.quests.road_name.AddRoadName
import de.westnordost.streetcomplete.quests.road_name.RoadNameSuggestionsSource
import de.westnordost.streetcomplete.quests.roof_shape.AddRoofShape
import de.westnordost.streetcomplete.quests.seating.AddSeating
import de.westnordost.streetcomplete.quests.segregated.AddCyclewaySegregation
import de.westnordost.streetcomplete.quests.self_service.AddSelfServiceLaundry
import de.westnordost.streetcomplete.quests.shop_type.CheckShopExistence
import de.westnordost.streetcomplete.quests.shop_type.CheckShopType
import de.westnordost.streetcomplete.quests.shop_type.SpecifyShopType
import de.westnordost.streetcomplete.quests.sidewalk.AddSidewalk
import de.westnordost.streetcomplete.quests.smoking.AddSmoking
import de.westnordost.streetcomplete.quests.smoothness.AddPathSmoothness
import de.westnordost.streetcomplete.quests.smoothness.AddRoadSmoothness
import de.westnordost.streetcomplete.quests.sport.AddSport
import de.westnordost.streetcomplete.quests.step_count.AddStepCount
import de.westnordost.streetcomplete.quests.step_count.AddStepCountStile
import de.westnordost.streetcomplete.quests.steps_ramp.AddStepsRamp
import de.westnordost.streetcomplete.quests.summit.AddSummitCross
import de.westnordost.streetcomplete.quests.summit.AddSummitRegister
import de.westnordost.streetcomplete.quests.surface.AddCyclewayPartSurface
import de.westnordost.streetcomplete.quests.surface.AddFootwayPartSurface
import de.westnordost.streetcomplete.quests.surface.AddPathSurface
import de.westnordost.streetcomplete.quests.surface.AddPitchSurface
import de.westnordost.streetcomplete.quests.surface.AddRoadSurface
import de.westnordost.streetcomplete.quests.surface.AddSidewalkSurface
import de.westnordost.streetcomplete.quests.tactile_paving.AddTactilePavingBusStop
import de.westnordost.streetcomplete.quests.tactile_paving.AddTactilePavingCrosswalk
import de.westnordost.streetcomplete.quests.tactile_paving.AddTactilePavingKerb
import de.westnordost.streetcomplete.quests.tactile_paving.AddTactilePavingSteps
import de.westnordost.streetcomplete.quests.toilet_availability.AddToiletAvailability
import de.westnordost.streetcomplete.quests.toilets_fee.AddToiletsFee
import de.westnordost.streetcomplete.quests.tourism_information.AddInformationToTourism
import de.westnordost.streetcomplete.quests.tracktype.AddTracktype
import de.westnordost.streetcomplete.quests.traffic_calming_type.AddTrafficCalmingType
import de.westnordost.streetcomplete.quests.traffic_signals_button.AddTrafficSignalsButton
import de.westnordost.streetcomplete.quests.traffic_signals_sound.AddTrafficSignalsSound
import de.westnordost.streetcomplete.quests.traffic_signals_vibrate.AddTrafficSignalsVibration
import de.westnordost.streetcomplete.quests.way_lit.AddWayLit
import de.westnordost.streetcomplete.quests.wheelchair_access.AddWheelchairAccessBusiness
import de.westnordost.streetcomplete.quests.wheelchair_access.AddWheelchairAccessOutside
import de.westnordost.streetcomplete.quests.wheelchair_access.AddWheelchairAccessPublicTransport
import de.westnordost.streetcomplete.quests.wheelchair_access.AddWheelchairAccessToilets
import de.westnordost.streetcomplete.quests.wheelchair_access.AddWheelchairAccessToiletsPart
import de.westnordost.streetcomplete.quests.width.AddCyclewayWidth
import de.westnordost.streetcomplete.quests.width.AddRoadWidth
import de.westnordost.streetcomplete.screens.measure.ArSupportChecker
import org.koin.core.qualifier.named
import org.koin.dsl.module
import java.util.concurrent.FutureTask


import de.westnordost.streetcomplete.quests.wdm.step_count.AddWDMStepCount
import de.westnordost.streetcomplete.quests.wdm.crossing_island.AddWDMCrossingIsland
import de.westnordost.streetcomplete.quests.wdm.wheelchair_access.AddWDMWheelchairAccessOutside
import de.westnordost.streetcomplete.quests.wdm.wheelchair_access.AddWDMWheelchairAccessOutsideLimitedNote
import de.westnordost.streetcomplete.quests.wdm.wheelchair_access.AddWDMWheelchairAccessStopPlace
import de.westnordost.streetcomplete.quests.wdm.wheelchair_access.AddWDMWheelchairAccessStopPlaceLimitedNote
import de.westnordost.streetcomplete.quests.wdm.wheelchair_access.AddWDMWheelchairAccessParkingBay
import de.westnordost.streetcomplete.quests.wdm.wheelchair_access.AddWDMWheelchairAccessParkingBayLimitedNote
import de.westnordost.streetcomplete.quests.wdm.wheelchair_access.AddWDMWheelchairAccessEntrance
import de.westnordost.streetcomplete.quests.wdm.wheelchair_access.AddWDMWheelchairAccessEntranceLimitedNote
import de.westnordost.streetcomplete.quests.wdm.wheelchair_access.AddWDMWheelchairAccessQuay
import de.westnordost.streetcomplete.quests.wdm.wheelchair_access.AddWDMWheelchairAccessQuayLimitedNote
import de.westnordost.streetcomplete.quests.wdm.wheelchair_access.AddWDMWheelchairAccessToilets
import de.westnordost.streetcomplete.quests.wdm.wheelchair_access.AddWDMWheelchairAccessToiletsLimitedNote
import de.westnordost.streetcomplete.quests.wdm.wheelchair_access.AddWDMWheelchairAccessErp
import de.westnordost.streetcomplete.quests.wdm.wheelchair_access.AddWDMWheelchairAccessErpLimitedNote
import de.westnordost.streetcomplete.quests.wdm.audible_signals.AddWDMAudibleSignals
import de.westnordost.streetcomplete.quests.wdm.audible_signals.AddWDMAudibleSignalsLimitedNote
import de.westnordost.streetcomplete.quests.wdm.visual_signs.AddWDMVisualSigns
import de.westnordost.streetcomplete.quests.wdm.visual_signs.AddWDMVisualSignsLimitedNote
import de.westnordost.streetcomplete.quests.wdm.audio_information.AddWDMAudioInformation
import de.westnordost.streetcomplete.quests.wdm.visual_displays.AddWDMVisualDisplays
import de.westnordost.streetcomplete.quests.wdm.tactile_guiding_strip.AddWDMTactileGuidingStrip
import de.westnordost.streetcomplete.quests.wdm.large_print_timetables.AddWDMLargePrintTimetables
import de.westnordost.streetcomplete.quests.wdm.real_time_departures.AddWDMRealTimeDepartures
import de.westnordost.streetcomplete.quests.wdm.assistance.AddWDMAssistances
import de.westnordost.streetcomplete.quests.wdm.ticket.AddWDMTickets
import de.westnordost.streetcomplete.quests.wdm.toilet.AddWDMToilets
import de.westnordost.streetcomplete.quests.wdm.width.AddWDMSitePathLinkWidth
import de.westnordost.streetcomplete.quests.wdm.tilt.AddWDMTilt
import de.westnordost.streetcomplete.quests.wdm.slope.AddWDMSlope
import de.westnordost.streetcomplete.quests.wdm.tilt.AddWDMTiltParkingBay
import de.westnordost.streetcomplete.quests.wdm.slope.AddWDMSlopeParkingBay
import de.westnordost.streetcomplete.quests.wdm.highway_type.AddWDMHighwayTypes
import de.westnordost.streetcomplete.quests.wdm.structure_type.AddWDMStructureTypes
import de.westnordost.streetcomplete.quests.wdm.outdoor.AddWDMOutdoors
import de.westnordost.streetcomplete.quests.wdm.incline.AddWDMInclines
import de.westnordost.streetcomplete.quests.wdm.lighting.AddWDMLightings
import de.westnordost.streetcomplete.quests.wdm.street_name.AddWDMStreetName
import de.westnordost.streetcomplete.quests.wdm.flooring_material.AddWDMFlooringMaterial
import de.westnordost.streetcomplete.quests.wdm.flooring_material.AddWDMFlooringMaterialParkingBay
import de.westnordost.streetcomplete.quests.wdm.flooring.AddWDMFlooring
import de.westnordost.streetcomplete.quests.wdm.flooring.AddWDMFlooringParkingBay
import de.westnordost.streetcomplete.quests.wdm.crossing.AddWDMCrossing
import de.westnordost.streetcomplete.quests.wdm.zebra_crossing.AddWDMZebraCrossing
import de.westnordost.streetcomplete.quests.wdm.pedestrian_lights.AddWDMPedestrianLights
import de.westnordost.streetcomplete.quests.wdm.visual_guidance_bands.AddWDMVisualGuidanceBands
import de.westnordost.streetcomplete.quests.wdm.acoustic_crossing_aids.AddWDMAcousticCrossingAids
import de.westnordost.streetcomplete.quests.wdm.audible_signals.AddWDMAudibleSignalsErp
import de.westnordost.streetcomplete.quests.wdm.audible_signals.AddWDMAudibleSignalsErpLimitedNote
import de.westnordost.streetcomplete.quests.wdm.audible_signals.AddWDMAudibleSignalsHall
import de.westnordost.streetcomplete.quests.wdm.audible_signals.AddWDMAudibleSignalsHallLimitedNote
import de.westnordost.streetcomplete.quests.wdm.automatic_door.AddWDMAudioAnnouncements
import de.westnordost.streetcomplete.quests.wdm.automatic_door.AddWDMAutomaticDoor
import de.westnordost.streetcomplete.quests.wdm.baby_change.AddWDMBabyChange
import de.westnordost.streetcomplete.quests.wdm.baby_change.AddWDMBabyChangeErp
import de.westnordost.streetcomplete.quests.wdm.baby_change.AddWDMBabyChangeToilets
import de.westnordost.streetcomplete.quests.wdm.depth.AddWDMElevatorDepth
import de.westnordost.streetcomplete.quests.wdm.entrance_passage.AddWDMEntrancePassage
import de.westnordost.streetcomplete.quests.wdm.escalator_free_access.AddWDMEscalatorFreeAccess
import de.westnordost.streetcomplete.quests.wdm.tactile_warning_strip.AddWDMTactileWarningStrip
import de.westnordost.streetcomplete.quests.wdm.tactile_warning_strip.AddWDMTactileWarningStripStairs
import de.westnordost.streetcomplete.quests.wdm.tactile_warning_strip.AddWDMTactileWarningStripKerb
import de.westnordost.streetcomplete.quests.wdm.kerb_design.AddWDMKerbDesign
import de.westnordost.streetcomplete.quests.wdm.step_free_access.AddWDMParkingBayStepFreeAccess
import de.westnordost.streetcomplete.quests.wdm.step_free_access.AddWDMStopPlaceStepFreeAccess
import de.westnordost.streetcomplete.quests.wdm.kerb_design.AddWDMKerbHeight
import de.westnordost.streetcomplete.quests.wdm.kerb_design.AddWDMKerbHeightEntrance
import de.westnordost.streetcomplete.quests.wdm.kerb_design.AddWDMKerbHeightKerb
import de.westnordost.streetcomplete.quests.wdm.necessary_force_to_open.AddWDMNecessaryForceToOpen
import de.westnordost.streetcomplete.quests.wdm.parking_visibility.AddWDMParkingVisibility
import de.westnordost.streetcomplete.quests.wdm.shower.AddWDMShower
import de.westnordost.streetcomplete.quests.wdm.shower.AddWDMShowerErp
import de.westnordost.streetcomplete.quests.wdm.shower.AddWDMShowerToilets
import de.westnordost.streetcomplete.quests.wdm.stop_place.AddWDMStopPlace
import de.westnordost.streetcomplete.quests.wdm.toilet.AddWDMToiletsErp
import de.westnordost.streetcomplete.quests.wdm.visual_displays.AddWDMElevatorVisualDisplays
import de.westnordost.streetcomplete.quests.wdm.visual_signs.AddWDMVisualSignsErp
import de.westnordost.streetcomplete.quests.wdm.visual_signs.AddWDMVisualSignsErpLimitedNote
import de.westnordost.streetcomplete.quests.wdm.visual_signs.AddWDMVisualSignsHall
import de.westnordost.streetcomplete.quests.wdm.visual_signs.AddWDMVisualSignsHallLimitedNote
import de.westnordost.streetcomplete.quests.wdm.visual_signs.AddWDMVisualSignsStopPlace
import de.westnordost.streetcomplete.quests.wdm.visual_signs.AddWDMVisualSignsStopPlaceLimitedNote

//import de.westnordost.streetcomplete.quests.wdm.baby_changing_table.AddWDMBabyChangingTable

val questsModule = module {
    factory { RoadNameSuggestionsSource(get()) }
    factory { WayTrafficFlowDao(get()) }

    single { questTypeRegistry(
        get(),
        get(),
        get(named("FeatureDictionaryFuture")),
        get(),
        get(named("CountryBoundariesFuture")),
        get(),
    ) }
}

fun questTypeRegistry(
    trafficFlowSegmentsApi: TrafficFlowSegmentsApi,
    trafficFlowDao: WayTrafficFlowDao,
    featureDictionaryFuture: FutureTask<FeatureDictionary>,
    countryInfos: CountryInfos,
    countryBoundariesFuture: FutureTask<CountryBoundaries>,
    arSupportChecker: ArSupportChecker
) = QuestTypeRegistry(listOf(

    /* The quest types are primarily sorted by how easy they can be solved:
    1. quests that are solvable from a distance or while passing by (fast)
    2. quests that require to be right in front of it (e.g. because it is small, you need to
      look for it or read text)
    3. quests that require some exploration or walking around to check (e.g. walking down the
      whole road to find the cycleway is the same along the whole way)
    4. quests that require to go inside, i.e. deviate from your walking route by a lot just
      to solve the quest
    5. quests that come in heaps (are spammy) come last: e.g. building type etc.

    The ordering within this primary sort order shall be whatever is faster so solve first:

    a. Yes/No quests, easy selections first,
    b. number and text inputs later,
    c. complex inputs (opening hours, ...) last. Quests that e.g. often require the way to be
      split up first are in effect also slow to answer

    The order can be watered down somewhat if it means that quests that usually apply to the
    same elements are in direct succession because we want to avoid that users are half-done
    answering all the quests for one element and then can't solve the last anymore because it
    is visually obscured by another quest.

    Finally, importance of the quest can still play a factor, but only secondarily.

    ---

    Each quest is assigned an ordinal. This is used for serialization and is thus never changed,
    even if the quest's order is changed or new quests are added somewhere in the middle. Each new
    quest always gets a new sequential ordinal.

    */

    /* always first: notes - they mark a mistake in the data so potentially every quest for that
    element is based on wrong data while the note is not resolved */

    0 to OsmNoteQuestType,

    5 to AddWDMStopPlace(),

    21 to AddWDMWheelchairAccessStopPlace(),

    22 to AddWDMWheelchairAccessStopPlaceLimitedNote(),

    40 to AddWDMWheelchairAccessToilets(),

    41 to AddWDMWheelchairAccessToiletsLimitedNote(),

    47 to AddWDMWheelchairAccessErp(),

    48 to AddWDMWheelchairAccessErpLimitedNote(),

    60 to AddWDMSitePathLinkWidth(arSupportChecker),

    61 to AddWDMTilt(),

    62 to AddWDMInclines(),

    63 to AddWDMSlope(),

    64 to AddWDMHighwayTypes(),

    65 to AddWDMStructureTypes(),

    66 to AddWDMOutdoors(),

    68 to AddWDMLightings(),

    69 to AddWDMStreetName(),

    70 to AddWDMFlooringMaterial(),

    71 to AddWDMFlooringMaterialParkingBay(),

    72 to AddWDMFlooring(),

    73 to AddWDMFlooringParkingBay(),

    80 to AddWDMStepCount(),

    90 to AddWDMCrossingIsland(),

    91 to AddWDMCrossing(),

    92 to AddWDMZebraCrossing(),

    93 to AddWDMPedestrianLights(),

    94 to AddWDMVisualGuidanceBands(),

    95 to AddWDMAcousticCrossingAids(),

    100 to AddWDMAudibleSignals(),

    110 to AddWDMAudibleSignalsLimitedNote(),

    111 to AddWDMAudibleSignalsErp(),

    112 to AddWDMAudibleSignalsErpLimitedNote(),

    113 to AddWDMAudibleSignalsHall(),

    114 to AddWDMAudibleSignalsHallLimitedNote(),

    120 to AddWDMVisualSigns(),

    130 to AddWDMVisualSignsLimitedNote(),

    131 to AddWDMVisualSignsErp(),

    132 to AddWDMVisualSignsErpLimitedNote(),

    133 to AddWDMVisualSignsHall(),

    134 to AddWDMVisualSignsHallLimitedNote(),

    135 to AddWDMVisualSignsStopPlace(),

    136 to AddWDMVisualSignsStopPlaceLimitedNote(),

    140 to AddWDMAudioInformation(),

    150 to AddWDMVisualDisplays(),

    160 to AddWDMTactileGuidingStrip(),

    170 to AddWDMLargePrintTimetables(),

    180 to AddWDMRealTimeDepartures(),

    190 to AddWDMAssistances(),

    200 to AddWDMTickets(),

    210 to AddWDMToilets(),

    211 to AddWDMToiletsErp(),

    220 to AddWDMTactileWarningStrip(),

    221 to AddWDMTactileWarningStripStairs(),

    222 to AddWDMTactileWarningStripKerb(),

    230 to AddWDMKerbDesign(),

    240 to AddWDMKerbHeight(),

    241 to AddWDMKerbHeightEntrance(),

    242 to AddWDMKerbHeightKerb(),

    250 to AddWDMBabyChange(),

    251 to AddWDMBabyChangeToilets(),

    252 to AddWDMBabyChangeErp(),

    253 to AddWDMShower(),

    254 to AddWDMShowerToilets(),

    255 to AddWDMShowerErp(),

    260 to AddWDMTiltParkingBay(),

    270 to AddWDMSlopeParkingBay(),

    280 to AddWDMElevatorDepth(arSupportChecker),

    290 to AddWDMAutomaticDoor(),

    300 to AddWDMParkingBayStepFreeAccess(),

    310 to AddWDMStopPlaceStepFreeAccess(),

    320 to AddWDMElevatorVisualDisplays(),

    330 to AddWDMAudioAnnouncements(),

    340 to AddWDMEscalatorFreeAccess(),

    345 to AddWDMEntrancePassage(),

    350 to AddWDMNecessaryForceToOpen(),

    360 to AddWDMParkingVisibility(),

    410 to AddWDMWheelchairAccessOutside(),

    420 to AddWDMWheelchairAccessOutsideLimitedNote(),

    430 to AddWDMWheelchairAccessParkingBay(),

    431 to AddWDMWheelchairAccessParkingBayLimitedNote(),

    435 to AddWDMWheelchairAccessEntrance(),

    436 to AddWDMWheelchairAccessEntranceLimitedNote(),

    445 to AddWDMWheelchairAccessQuay(),

    446 to AddWDMWheelchairAccessQuayLimitedNote(),


))
