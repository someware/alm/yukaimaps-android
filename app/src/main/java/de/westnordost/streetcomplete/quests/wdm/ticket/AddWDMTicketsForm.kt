package de.westnordost.streetcomplete.quests.wdm.ticket

import android.os.Bundle
import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.quests.AImageListQuestForm

class AddWDMTicketsForm :
    AImageListQuestForm<List<WDMTicket>, WDMTicketsAnswer>() {

    override val descriptionResId = R.string.quest_wdm_tickets_note

    override val items get() = WDMTicket.selectableValues.map { it.asItem() }

    override val maxSelectableItems = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        imageSelector.cellLayoutId = R.layout.cell_icon_select_with_label_below
    }

    override fun onClickOk(selectedItems: List<List<WDMTicket>>) {
        applyAnswer(WDMTickets(selectedItems.flatten()))
    }
}
