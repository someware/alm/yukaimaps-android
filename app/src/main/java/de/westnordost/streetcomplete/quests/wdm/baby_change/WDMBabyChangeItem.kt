package de.westnordost.streetcomplete.quests.wdm.baby_change

import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.quests.wdm.baby_change.WDMBabyChange.PMR
import de.westnordost.streetcomplete.quests.wdm.baby_change.WDMBabyChange.NOPMR
import de.westnordost.streetcomplete.quests.wdm.baby_change.WDMBabyChange.NONE
import de.westnordost.streetcomplete.view.image_select.Item

fun WDMBabyChange.asItem(): Item<List<WDMBabyChange>> =
    Item(listOf(this), iconResId, titleResId)

private val WDMBabyChange.iconResId: Int get() = when (this) {
    PMR ->     R.drawable.wdm_baby_change_pmr
    NOPMR ->             R.drawable.wdm_default_photo  //TODO
    NONE ->             R.drawable.wdm_default_photo  //TODO
}

private val WDMBabyChange.titleResId: Int get() = when (this) {
    PMR ->     R.string.quest_wdm_baby_change_pmr
    NOPMR ->             R.string.quest_wdm_baby_change_nopmr
    NONE ->             R.string.quest_wdm_baby_change_none
}
