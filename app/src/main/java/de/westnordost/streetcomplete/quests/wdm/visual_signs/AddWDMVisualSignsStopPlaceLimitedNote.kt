package de.westnordost.streetcomplete.quests.wdm.visual_signs

import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.data.osm.geometry.ElementGeometry
import de.westnordost.streetcomplete.data.osm.osmquests.OsmFilterQuestType
import de.westnordost.streetcomplete.data.user.achievements.EditTypeAchievement.WHEELCHAIR
import de.westnordost.streetcomplete.osm.Tags

class AddWDMVisualSignsStopPlaceLimitedNote : OsmFilterQuestType<String>() {

    override val elementFilter = """
        nodes, ways with PointOfInterest = StopPlace
        and VisualSigns = limited
        and !VisualSigns:Description
    """
    override val changesetComment = "WDM Survey stop place limited note visual signs"
    override val wikiLink = null
    override val icon = R.drawable.ic_wdm_visual_signs_erp
    override val achievements = listOf(WHEELCHAIR)

    override fun getTitle(tags: Map<String, String>) = R.string.quest_wdm_visual_signs_limited_note_stop_place_title

    override fun createForm() = AddWDMVisualSignsLimitedNoteForm()

    override fun applyAnswerTo(answer: String, tags: Tags, geometry: ElementGeometry, timestampEdited: Long) {
        tags["VisualSigns:Description"] = answer
    }
}
