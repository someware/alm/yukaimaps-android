package de.westnordost.streetcomplete.quests.wdm.structure_type

import android.os.Bundle
import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.quests.AImageListQuestForm

class AddWDMStructureTypesForm :
    AImageListQuestForm<List<WDMStructureType>, WDMStructureTypesAnswer>() {

    override val descriptionResId = R.string.quest_wdm_structure_type_note

    override val items get() = WDMStructureType.selectableValues.map { it.asItem() }

    override val maxSelectableItems = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        imageSelector.cellLayoutId = R.layout.cell_icon_select_with_label_below
    }

    override fun onClickOk(selectedItems: List<List<WDMStructureType>>) {
        applyAnswer(WDMStructureTypes(selectedItems.flatten()))
    }
}
