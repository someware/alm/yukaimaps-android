package de.westnordost.streetcomplete.quests.wdm.assistance

import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.quests.wdm.assistance.WDMAssistance.BOARDING
import de.westnordost.streetcomplete.quests.wdm.assistance.WDMAssistance.WHEELCHAIR
import de.westnordost.streetcomplete.quests.wdm.assistance.WDMAssistance.CHILD
import de.westnordost.streetcomplete.quests.wdm.assistance.WDMAssistance.INFORMATION
import de.westnordost.streetcomplete.quests.wdm.assistance.WDMAssistance.STATION_MASTER
import de.westnordost.streetcomplete.quests.wdm.assistance.WDMAssistance.NONE
import de.westnordost.streetcomplete.view.image_select.Item

fun WDMAssistance.asItem(): Item<List<WDMAssistance>> =
    Item(listOf(this), iconResId, titleResId)

private val WDMAssistance.iconResId: Int get() = when (this) {
    BOARDING ->     R.drawable.ic_wdm_assistance_boarding
    WHEELCHAIR ->             R.drawable.ic_wdm_assistance_wheelchair
    CHILD ->             R.drawable.ic_wdm_assistance_child
    INFORMATION ->           R.drawable.ic_wdm_assistance_information
    STATION_MASTER -> R.drawable.ic_wdm_assistance_station_master
    NONE ->   R.drawable.ic_quest_notes //TODO
}

private val WDMAssistance.titleResId: Int get() = when (this) {
    BOARDING ->     R.string.quest_wdm_assistance_boarding
    WHEELCHAIR ->             R.string.quest_wdm_assistance_wheelchair
    CHILD ->             R.string.quest_wdm_assistance_child
    INFORMATION ->           R.string.quest_wdm_assistance_information
    STATION_MASTER -> R.string.quest_wdm_assistance_station_master
    NONE ->               R.string.quest_wdm_assistance_none
}
