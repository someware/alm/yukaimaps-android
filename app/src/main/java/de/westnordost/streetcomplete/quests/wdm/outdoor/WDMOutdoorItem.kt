package de.westnordost.streetcomplete.quests.wdm.outdoor

import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.quests.wdm.outdoor.WDMOutdoor.YES
import de.westnordost.streetcomplete.quests.wdm.outdoor.WDMOutdoor.NO
import de.westnordost.streetcomplete.quests.wdm.outdoor.WDMOutdoor.COVERED
import de.westnordost.streetcomplete.view.image_select.Item

fun WDMOutdoor.asItem(): Item<List<WDMOutdoor>> =
    Item(listOf(this), iconResId, titleResId)

private val WDMOutdoor.iconResId: Int get() = when (this) {
    YES ->     R.drawable.wdm_outdoor_yes
    NO ->             R.drawable.wdm_outdoor_no
    COVERED ->             R.drawable.wdm_outdoor_covered
}

private val WDMOutdoor.titleResId: Int get() = when (this) {
    YES ->     R.string.quest_wdm_outdoor_yes
    NO ->             R.string.quest_wdm_outdoor_no
    COVERED ->             R.string.quest_wdm_outdoor_covered
}
