package de.westnordost.streetcomplete.quests.wdm.audio_information

import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.quests.AbstractOsmQuestForm
import de.westnordost.streetcomplete.quests.AnswerItem
import de.westnordost.streetcomplete.quests.wdm.audio_information.WDMAudioInformation.ADAPTED
import de.westnordost.streetcomplete.quests.wdm.audio_information.WDMAudioInformation.UNADAPTED
import de.westnordost.streetcomplete.quests.wdm.audio_information.WDMAudioInformation.NO

open class WDMAudioInformationForm : AbstractOsmQuestForm<WDMAudioInformation>() {

    override val buttonPanelAnswers = listOf(
        AnswerItem(R.string.quest_generic_hasFeature_no) { applyAnswer(NO) },
        AnswerItem(R.string.quest_wdm_audio_information_adapted) { applyAnswer(ADAPTED) },
        AnswerItem(R.string.quest_wdm_audio_information_unadapted) {
            applyAnswer(UNADAPTED)
        },
    )
}
