package de.westnordost.streetcomplete.quests.wdm.depth

import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.data.elementfilter.toElementFilterExpression
import de.westnordost.streetcomplete.data.osm.geometry.ElementGeometry
import de.westnordost.streetcomplete.data.osm.mapdata.Element
import de.westnordost.streetcomplete.data.osm.mapdata.MapDataWithGeometry
import de.westnordost.streetcomplete.data.osm.mapdata.filter
import de.westnordost.streetcomplete.data.osm.osmquests.OsmElementQuestType
import de.westnordost.streetcomplete.data.user.achievements.EditTypeAchievement
import de.westnordost.streetcomplete.osm.MAXSPEED_TYPE_KEYS
import de.westnordost.streetcomplete.osm.ROADS_ASSUMED_TO_BE_PAVED
import de.westnordost.streetcomplete.osm.Tags
import de.westnordost.streetcomplete.osm.surface.ANYTHING_PAVED
import de.westnordost.streetcomplete.screens.measure.ArSupportChecker

class AddWDMElevatorDepth(
    private val checkArSupport: ArSupportChecker
) : OsmElementQuestType<WDMDepthAnswer> {

    private val wayFilter by lazy { """
        nodes, ways with SitePathLink = Elevator
        and !Depth
    """.toElementFilterExpression() }

    override val changesetComment = "WDM Determine elevator depths"
    override val wikiLink = null
    override val icon = R.drawable.ic_quest_wdm_elevator_depth
    override val achievements = listOf(EditTypeAchievement.WHEELCHAIR)
    override val defaultDisabledMessage: Int
        get() = if (!checkArSupport()) R.string.default_disabled_msg_no_ar else 0

    override fun getTitle(tags: Map<String, String>) =
        R.string.quest_wdm_elevator_depth_title

    override fun getApplicableElements(mapData: MapDataWithGeometry) =
        mapData.ways.filter { wayFilter.matches(it) }

    override fun isApplicableTo(element: Element) =
        wayFilter.matches(element)

    override fun createForm() = AddWDMDepthForm()

    override fun applyAnswerTo(answer: WDMDepthAnswer, tags: Tags, geometry: ElementGeometry, timestampEdited: Long) {
        val key = "Depth"

        tags[key] = answer.width.toOsmValue()

        if (answer.isARMeasurement) {
            tags["source:$key"] = "ARCore"
        } else {
            tags.remove("source:$key")
        }
    }
}
