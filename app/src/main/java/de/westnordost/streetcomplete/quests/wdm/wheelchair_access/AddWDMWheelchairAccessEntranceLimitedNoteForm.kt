package de.westnordost.streetcomplete.quests.wdm.wheelchair_access

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.core.widget.doAfterTextChanged
import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.databinding.QuestWdmWheelchairEntranceLimitedNoteBinding
import de.westnordost.streetcomplete.quests.AbstractOsmQuestForm
import de.westnordost.streetcomplete.util.ktx.nonBlankTextOrNull

class AddWDMWheelchairAccessEntranceLimitedNoteForm : AbstractOsmQuestForm<String>() {

    override val contentLayoutResId = R.layout.quest_wdm_wheelchair_entrance_limited_note
    private val binding by contentViewBinding(QuestWdmWheelchairEntranceLimitedNoteBinding::bind)

    private val noteText: String? get() = binding.noteInput.nonBlankTextOrNull

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.noteInput.doAfterTextChanged { checkIsFormComplete() }
    }

    override fun isFormComplete() = noteText != null

    override fun onClickOk() {
        applyAnswer(noteText.toString())
    }

    companion object {
        private const val ARG_DESCRIPTION = "description"

        fun create(descriptionResId: Int): AddWDMWheelchairAccessEntranceLimitedNoteForm {
            val form = AddWDMWheelchairAccessEntranceLimitedNoteForm()
            form.arguments = bundleOf(ARG_DESCRIPTION to descriptionResId)
            return form
        }
    }
}
