package de.westnordost.streetcomplete.quests.wdm.street_name

import de.westnordost.osmfeatures.FeatureDictionary
import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.data.elementfilter.toElementFilterExpression
import de.westnordost.streetcomplete.data.osm.geometry.ElementGeometry
import de.westnordost.streetcomplete.data.osm.mapdata.Element
import de.westnordost.streetcomplete.data.osm.mapdata.MapDataWithGeometry
import de.westnordost.streetcomplete.data.osm.mapdata.filter
import de.westnordost.streetcomplete.data.osm.osmquests.OsmElementQuestType
import de.westnordost.streetcomplete.data.user.achievements.EditTypeAchievement
import de.westnordost.streetcomplete.data.user.achievements.EditTypeAchievement.CITIZEN
import de.westnordost.streetcomplete.osm.IS_SHOP_OR_DISUSED_SHOP_EXPRESSION
import de.westnordost.streetcomplete.osm.Tags
import de.westnordost.streetcomplete.osm.applyTo
import de.westnordost.streetcomplete.quests.place_name.NoPlaceNameSign
import de.westnordost.streetcomplete.quests.place_name.PlaceName
import de.westnordost.streetcomplete.util.ktx.toYesNo
import java.util.concurrent.FutureTask

class AddWDMStreetName() : OsmElementQuestType<WDMStreetNameAnswer> {

    private val filter by lazy { """
        ways with SitePathLink
        and SitePathLink = Street
        and !Name
    """.toElementFilterExpression() }

    override val changesetComment = "Determine street names"
    override val wikiLink = null
    override val icon = R.drawable.ic_quest_wdm__street_name
    override val isReplaceShopEnabled = true
    override val achievements = listOf(EditTypeAchievement.WHEELCHAIR)

    override fun getTitle(tags: Map<String, String>) = R.string.quest_wdm_street_name_title

    override fun getApplicableElements(mapData: MapDataWithGeometry): Iterable<Element> =
        mapData.filter { isApplicableTo(it) }

    override fun isApplicableTo(element: Element): Boolean =
        filter.matches(element)

    override fun createForm() = AddWDMStreetNameForm()

    override fun applyAnswerTo(answer: WDMStreetNameAnswer, tags: Tags, geometry: ElementGeometry, timestampEdited: Long) {
        when (answer) {
            is WDMNoStreetNameSign -> {
                //tags["Name"] = "no"
            }
            is WDMStreetName -> {
                tags["Name"] = answer.localizedNames[0].name
            }
        }
    }
}
