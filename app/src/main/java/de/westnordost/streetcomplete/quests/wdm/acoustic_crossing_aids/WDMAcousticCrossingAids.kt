package de.westnordost.streetcomplete.quests.wdm.acoustic_crossing_aids

enum class WDMAcousticCrossingAids {
    NONE,
    GOOD,
    WORN,
    DISCOMFORTABLE,
    HAZARDOUS
}
