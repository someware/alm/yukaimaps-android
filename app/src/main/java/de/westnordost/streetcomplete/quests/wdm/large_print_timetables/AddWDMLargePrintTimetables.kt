package de.westnordost.streetcomplete.quests.wdm.large_print_timetables

import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.data.osm.geometry.ElementGeometry
import de.westnordost.streetcomplete.data.osm.osmquests.OsmFilterQuestType
import de.westnordost.streetcomplete.data.user.achievements.EditTypeAchievement
import de.westnordost.streetcomplete.osm.Tags
import de.westnordost.streetcomplete.osm.updateWithCheckDate
import de.westnordost.streetcomplete.util.ktx.toYesNo

class AddWDMLargePrintTimetables : OsmFilterQuestType<Boolean>() {

    override val elementFilter = """
        nodes, ways with PointOfInterest = StopPlace
        and !LargePrintTimetables
    """

    override val changesetComment = "WDM Survey large print timetables"
    override val wikiLink = null
    override val icon = R.drawable.ic_wdm_large_print_timetables
    override val achievements = listOf(EditTypeAchievement.WHEELCHAIR)

    override fun getTitle(tags: Map<String, String>) = R.string.quest_wdm_large_print_timetables_title

    override fun createForm() = WDMLargePrintTimetablesForm()

    override fun applyAnswerTo(answer: Boolean, tags: Tags, geometry: ElementGeometry, timestampEdited: Long) {
        tags.updateWithCheckDate("LargePrintTimetables", answer.toYesNo())
    }
}
