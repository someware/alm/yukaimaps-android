package de.westnordost.streetcomplete.quests.wdm.ticket

/** All assistance:* keys known to StreetComplete */
enum class WDMTicket(val value: String) {
    MACHINE("machine"),
    OFFICE("office"),
    PRINT("print"),
    MOBILE("mobile");

    companion object {
        val selectableValues = listOf(
            MACHINE, OFFICE, PRINT, MOBILE
        )
    }
}
