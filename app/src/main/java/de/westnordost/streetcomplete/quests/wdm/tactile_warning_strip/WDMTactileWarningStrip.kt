package de.westnordost.streetcomplete.quests.wdm.tactile_warning_strip

enum class WDMTactileWarningStrip {
    NONE,
    GOOD,
    WORN,
    DISCOMFORTABLE,
    HAZARDOUS
}
