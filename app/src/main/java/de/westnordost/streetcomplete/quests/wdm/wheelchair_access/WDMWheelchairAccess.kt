package de.westnordost.streetcomplete.quests.wdm.wheelchair_access

enum class WDMWheelchairAccess(val osmValue: String) {
    YES("yes"),
    LIMITED("limited"),
    NO("no"),
}
