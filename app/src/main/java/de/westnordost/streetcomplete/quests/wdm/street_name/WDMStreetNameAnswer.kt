package de.westnordost.streetcomplete.quests.wdm.street_name

import de.westnordost.streetcomplete.osm.LocalizedName

sealed interface WDMStreetNameAnswer

data class WDMStreetName(val localizedNames: List<LocalizedName>) : WDMStreetNameAnswer
object WDMNoStreetNameSign : WDMStreetNameAnswer
