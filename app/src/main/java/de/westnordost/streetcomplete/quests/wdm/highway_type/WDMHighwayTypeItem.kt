package de.westnordost.streetcomplete.quests.wdm.highway_type

import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.quests.wdm.highway_type.WDMHighwayType.STREET
import de.westnordost.streetcomplete.quests.wdm.highway_type.WDMHighwayType.LIMITEDSPEEDSTREET
import de.westnordost.streetcomplete.quests.wdm.highway_type.WDMHighwayType.LIVINGSTREET
import de.westnordost.streetcomplete.quests.wdm.highway_type.WDMHighwayType.PEDESTRIAN
import de.westnordost.streetcomplete.quests.wdm.highway_type.WDMHighwayType.BICYCLE
import de.westnordost.streetcomplete.quests.wdm.highway_type.WDMHighwayType.UNCLASSIFIED
import de.westnordost.streetcomplete.view.image_select.Item

fun WDMHighwayType.asItem(): Item<List<WDMHighwayType>> =
    Item(listOf(this), iconResId, titleResId)

private val WDMHighwayType.iconResId: Int get() = when (this) {
    STREET ->     R.drawable.ic_wdm_highway_type_street
    LIMITEDSPEEDSTREET ->             R.drawable.ic_wdm_highway_type_limited_speed_street
    LIVINGSTREET ->             R.drawable.ic_wdm_highway_type_living_street
    PEDESTRIAN ->           R.drawable.ic_wdm_highway_type_pedestrian
    BICYCLE -> R.drawable.ic_wdm_highway_type_bicycle
    UNCLASSIFIED ->   R.drawable.ic_quest_notes //TODO
}

private val WDMHighwayType.titleResId: Int get() = when (this) {
    STREET ->     R.string.quest_wdm_highway_type_street
    LIMITEDSPEEDSTREET ->             R.string.quest_wdm_highway_type_limited_speed_street
    LIVINGSTREET ->             R.string.quest_wdm_highway_type_living_street
    PEDESTRIAN ->           R.string.quest_wdm_highway_type_pedestrian
    BICYCLE -> R.string.quest_wdm_highway_type_station_bicycle
    UNCLASSIFIED ->               R.string.quest_wdm_highway_type_unclassified
}
