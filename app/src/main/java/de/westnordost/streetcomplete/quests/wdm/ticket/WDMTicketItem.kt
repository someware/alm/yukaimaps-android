package de.westnordost.streetcomplete.quests.wdm.ticket

import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.quests.wdm.ticket.WDMTicket.MACHINE
import de.westnordost.streetcomplete.quests.wdm.ticket.WDMTicket.OFFICE
import de.westnordost.streetcomplete.quests.wdm.ticket.WDMTicket.PRINT
import de.westnordost.streetcomplete.quests.wdm.ticket.WDMTicket.MOBILE
import de.westnordost.streetcomplete.view.image_select.Item

fun WDMTicket.asItem(): Item<List<WDMTicket>> =
    Item(listOf(this), iconResId, titleResId)

private val WDMTicket.iconResId: Int get() = when (this) {
    MACHINE ->     R.drawable.ic_wdm_tickets_machine
    OFFICE ->             R.drawable.ic_wdm_tickets_office
    PRINT ->             R.drawable.ic_wdm_tickets_print //TODO
    MOBILE ->           R.drawable.ic_wdm_tickets_mobile
}

private val WDMTicket.titleResId: Int get() = when (this) {
    MACHINE ->     R.string.quest_wdm_tickets_machine
    OFFICE ->             R.string.quest_wdm_tickets_office
    PRINT ->             R.string.quest_wdm_tickets_print
    MOBILE ->           R.string.quest_wdm_tickets_mobile
}
