package de.westnordost.streetcomplete.quests.wdm.flooring

enum class WDMFlooring {
    NONE,
    GOOD,
    WORN,
    DISCOMFORTABLE,
    HAZARDOUS
}
