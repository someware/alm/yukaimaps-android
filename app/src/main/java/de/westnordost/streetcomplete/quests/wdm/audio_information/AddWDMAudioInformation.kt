package de.westnordost.streetcomplete.quests.wdm.audio_information

import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.data.osm.geometry.ElementGeometry
import de.westnordost.streetcomplete.data.osm.osmquests.OsmFilterQuestType
import de.westnordost.streetcomplete.data.user.achievements.EditTypeAchievement.WHEELCHAIR
import de.westnordost.streetcomplete.osm.Tags

class AddWDMAudioInformation : OsmFilterQuestType<WDMAudioInformation>() {

    override val elementFilter = """
        nodes, ways with PointOfInterest = StopPlace
        and !AudioInformation
    """
    override val changesetComment = "WDM Survey audio information"
    override val wikiLink = null
    override val icon = R.drawable.ic_wdm_audio_information
    override val achievements = listOf(WHEELCHAIR)

    override fun getTitle(tags: Map<String, String>) = R.string.quest_wdm_audio_information_title

    override fun createForm() = AddWDMAudioInformationForm()

    override fun applyAnswerTo(answer: WDMAudioInformation, tags: Tags, geometry: ElementGeometry, timestampEdited: Long) {
        tags["AudioInformation"] = answer.osmValue
    }
}
