package de.westnordost.streetcomplete.quests.wdm.stop_place

import de.westnordost.streetcomplete.quests.AImageListQuestForm

class AddWDMStopPlaceForm : AImageListQuestForm<WDMStopPlace, WDMStopPlace>() {

    override val items = WDMStopPlace.values().map { it.asItem() }
    override val itemsPerRow = 3

    override fun onClickOk(selectedItems: List<WDMStopPlace>) {
        applyAnswer(selectedItems.single())
    }
}
