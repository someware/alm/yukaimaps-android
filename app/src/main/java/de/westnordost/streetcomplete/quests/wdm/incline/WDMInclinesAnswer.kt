package de.westnordost.streetcomplete.quests.wdm.incline

sealed interface WDMInclinesAnswer

data class WDMInclines(val inclines: List<WDMIncline>) : WDMInclinesAnswer
