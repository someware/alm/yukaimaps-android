package de.westnordost.streetcomplete.quests.wdm.crossing_island

import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.data.osm.geometry.ElementGeometry
import de.westnordost.streetcomplete.data.osm.osmquests.OsmFilterQuestType
import de.westnordost.streetcomplete.data.quest.AllCountriesExcept
import de.westnordost.streetcomplete.data.user.achievements.EditTypeAchievement
import de.westnordost.streetcomplete.data.user.achievements.EditTypeAchievement.CITIZEN
import de.westnordost.streetcomplete.osm.Tags
import de.westnordost.streetcomplete.quests.YesNoQuestForm
import de.westnordost.streetcomplete.util.ktx.toYesNo

class AddWDMCrossingIsland : OsmFilterQuestType<Boolean>() {

    override val elementFilter = """
        ways with SitePathLink = Crossing
         and !CrossingIsland
    """

    override val changesetComment = "Specify crossing islands"
    override val wikiLink = null
    override val icon = R.drawable.ic_quest_wdm_pedestrian_crossing_island
    override val achievements = listOf(EditTypeAchievement.WHEELCHAIR)

    override fun getTitle(tags: Map<String, String>) = R.string.quest_wdm_crossing_island_title

    override fun createForm() = YesNoQuestForm()

    override fun applyAnswerTo(answer: Boolean, tags: Tags, geometry: ElementGeometry, timestampEdited: Long) {
        tags["CrossingIsland"] = answer.toYesNo()
    }
}
