package de.westnordost.streetcomplete.quests.wdm.outdoor

sealed interface WDMOutdoorsAnswer

data class WDMOutdoors(val outdoors: List<WDMOutdoor>) : WDMOutdoorsAnswer
