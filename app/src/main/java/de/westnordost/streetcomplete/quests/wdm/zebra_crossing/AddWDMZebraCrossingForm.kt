package de.westnordost.streetcomplete.quests.wdm.zebra_crossing

import de.westnordost.streetcomplete.quests.AImageListQuestForm

class AddWDMZebraCrossingForm : AImageListQuestForm<WDMZebraCrossing, WDMZebraCrossing>() {

    override val items = WDMZebraCrossing.values().map { it.asItem() }
    override val itemsPerRow = 3

    override fun onClickOk(selectedItems: List<WDMZebraCrossing>) {
        applyAnswer(selectedItems.single())
    }
}
