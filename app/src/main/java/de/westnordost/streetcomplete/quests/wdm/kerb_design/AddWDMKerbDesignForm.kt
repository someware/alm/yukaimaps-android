package de.westnordost.streetcomplete.quests.wdm.kerb_design

import de.westnordost.streetcomplete.quests.AImageListQuestForm

class AddWDMKerbDesignForm : AImageListQuestForm<WDMKerbDesign, WDMKerbDesign>() {

    override val items = WDMKerbDesign.values().map { it.asItem() }
    override val itemsPerRow = 2
    override val moveFavoritesToFront = false

    override fun onClickOk(selectedItems: List<WDMKerbDesign>) {
        applyAnswer(selectedItems.single())
    }
}
