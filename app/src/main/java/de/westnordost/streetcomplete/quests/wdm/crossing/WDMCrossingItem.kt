package de.westnordost.streetcomplete.quests.wdm.crossing

import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.quests.wdm.crossing.WDMCrossing.RAIL
import de.westnordost.streetcomplete.quests.wdm.crossing.WDMCrossing.URBANRAIL
import de.westnordost.streetcomplete.quests.wdm.crossing.WDMCrossing.ROAD
import de.westnordost.streetcomplete.quests.wdm.crossing.WDMCrossing.CYCLEWAY
import de.westnordost.streetcomplete.view.image_select.Item

fun WDMCrossing.asItem() = Item(this, iconResId, titleResId)

private val WDMCrossing.titleResId: Int get() = when (this) {
    RAIL ->  R.string.quest_wdm_crossing_rail
    URBANRAIL -> R.string.quest_wdm_crossing_urban_rail
    ROAD ->      R.string.quest_wdm_crossing_road
    CYCLEWAY ->      R.string.quest_wdm_crossing_cycleway
}

private val WDMCrossing.iconResId: Int get() = when (this) {
    RAIL ->  R.drawable.wdm_crossing_rail
    URBANRAIL -> R.drawable.wdm_crossing_urban_rail
    ROAD ->      R.drawable.wdm_crossing_road
    CYCLEWAY ->      R.drawable.wdm_crossing_cycleway
}
