package de.westnordost.streetcomplete.quests.wdm.structure_type

import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.quests.wdm.structure_type.WDMStructureType.PATHWAY
import de.westnordost.streetcomplete.quests.wdm.structure_type.WDMStructureType.OVERPASS
import de.westnordost.streetcomplete.quests.wdm.structure_type.WDMStructureType.UNDERPASS
import de.westnordost.streetcomplete.quests.wdm.structure_type.WDMStructureType.TUNNEL
import de.westnordost.streetcomplete.quests.wdm.structure_type.WDMStructureType.CORRIDOR
import de.westnordost.streetcomplete.view.image_select.Item

fun WDMStructureType.asItem(): Item<List<WDMStructureType>> =
    Item(listOf(this), iconResId, titleResId)

private val WDMStructureType.iconResId: Int get() = when (this) {
    PATHWAY ->     R.drawable.wdm_structure_type_pathway
    OVERPASS ->             R.drawable.wdm_structure_type_overpass
    UNDERPASS ->             R.drawable.wdm_structure_type_underpass
    TUNNEL ->           R.drawable.wdm_structure_type_tunnel
    CORRIDOR -> R.drawable.wdm_structure_type_station_corridor
}

private val WDMStructureType.titleResId: Int get() = when (this) {
    PATHWAY ->     R.string.quest_wdm_structure_type_pathway
    OVERPASS ->             R.string.quest_wdm_structure_type_overpass
    UNDERPASS ->             R.string.quest_wdm_structure_type_underpass
    TUNNEL ->           R.string.quest_wdm_structure_type_tunnel
    CORRIDOR -> R.string.quest_wdm_structure_type_station_corridor
}
