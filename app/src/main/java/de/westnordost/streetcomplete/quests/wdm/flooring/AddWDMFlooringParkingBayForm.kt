package de.westnordost.streetcomplete.quests.wdm.flooring

import de.westnordost.streetcomplete.quests.AImageListQuestForm

class AddWDMFlooringParkingBayForm : AImageListQuestForm<WDMFlooring, WDMFlooring>() {

    override val items = WDMFlooring.values().map { it.asItem() }
    override val itemsPerRow = 3

    override fun onClickOk(selectedItems: List<WDMFlooring>) {
        applyAnswer(selectedItems.single())
    }
}
