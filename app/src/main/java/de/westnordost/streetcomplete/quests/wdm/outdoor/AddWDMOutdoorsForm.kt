package de.westnordost.streetcomplete.quests.wdm.outdoor

import android.os.Bundle
import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.quests.AImageListQuestForm

class AddWDMOutdoorsForm :
    AImageListQuestForm<List<WDMOutdoor>, WDMOutdoorsAnswer>() {

    override val descriptionResId = R.string.quest_wdm_outdoor_note

    override val items get() = WDMOutdoor.selectableValues.map { it.asItem() }

    override val maxSelectableItems = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        imageSelector.cellLayoutId = R.layout.cell_icon_select_with_label_below
    }

    override fun onClickOk(selectedItems: List<List<WDMOutdoor>>) {
        applyAnswer(WDMOutdoors(selectedItems.flatten()))
    }
}
