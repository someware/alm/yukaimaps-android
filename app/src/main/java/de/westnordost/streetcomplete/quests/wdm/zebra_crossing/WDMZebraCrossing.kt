package de.westnordost.streetcomplete.quests.wdm.zebra_crossing

enum class WDMZebraCrossing {
    NONE,
    GOOD,
    WORN,
    DISCOMFORTABLE,
    HAZARDOUS
}
