package de.westnordost.streetcomplete.quests.wdm.acoustic_crossing_aids

import de.westnordost.streetcomplete.quests.AImageListQuestForm

class AddWDMAcousticCrossingAidsForm : AImageListQuestForm<WDMAcousticCrossingAids, WDMAcousticCrossingAids>() {

    override val items = WDMAcousticCrossingAids.values().map { it.asItem() }
    override val itemsPerRow = 3

    override fun onClickOk(selectedItems: List<WDMAcousticCrossingAids>) {
        applyAnswer(selectedItems.single())
    }
}
