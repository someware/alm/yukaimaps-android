package de.westnordost.streetcomplete.quests.wdm.parking_visibility

import de.westnordost.streetcomplete.quests.AImageListQuestForm

class AddWDMParkingVisibilityForm : AImageListQuestForm<WDMParkingVisibility, WDMParkingVisibility>() {

    override val items = WDMParkingVisibility.values().map { it.asItem() }
    override val itemsPerRow = 3

    override fun onClickOk(selectedItems: List<WDMParkingVisibility>) {
        applyAnswer(selectedItems.single())
    }
}
