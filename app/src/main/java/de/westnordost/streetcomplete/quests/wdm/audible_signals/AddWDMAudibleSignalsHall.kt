package de.westnordost.streetcomplete.quests.wdm.audible_signals

import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.data.osm.geometry.ElementGeometry
import de.westnordost.streetcomplete.data.osm.osmquests.OsmFilterQuestType
import de.westnordost.streetcomplete.data.user.achievements.EditTypeAchievement.WHEELCHAIR
import de.westnordost.streetcomplete.osm.Tags

class AddWDMAudibleSignalsHall : OsmFilterQuestType<WDMAudibleSignals>() {

    override val elementFilter = """
        ways with SitePathLink
        and SitePathLink ~ Hall|Corridor
        and !AudibleSignals
    """
    override val changesetComment = "WDM Survey hall audible signals"
    override val wikiLink = null
    override val icon = R.drawable.ic_quest_wdm_audio_announcements
    override val achievements = listOf(WHEELCHAIR)

    override fun getTitle(tags: Map<String, String>) = R.string.quest_wdm_audible_signals_hall_title

    override fun createForm() = AddWDMAudibleSignalsErpForm()

    override fun applyAnswerTo(answer: WDMAudibleSignals, tags: Tags, geometry: ElementGeometry, timestampEdited: Long) {
        tags["AudibleSignals"] = answer.osmValue
    }
}
