package de.westnordost.streetcomplete.quests.wdm.zebra_crossing

import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.quests.wdm.zebra_crossing.WDMZebraCrossing.NONE
import de.westnordost.streetcomplete.quests.wdm.zebra_crossing.WDMZebraCrossing.GOOD
import de.westnordost.streetcomplete.quests.wdm.zebra_crossing.WDMZebraCrossing.WORN
import de.westnordost.streetcomplete.quests.wdm.zebra_crossing.WDMZebraCrossing.DISCOMFORTABLE
import de.westnordost.streetcomplete.quests.wdm.zebra_crossing.WDMZebraCrossing.HAZARDOUS
import de.westnordost.streetcomplete.view.image_select.Item

fun WDMZebraCrossing.asItem() = Item(this, iconResId, titleResId)

private val WDMZebraCrossing.titleResId: Int get() = when (this) {
    NONE ->  R.string.quest_wdm_zebra_crossing_none
    GOOD -> R.string.quest_wdm_zebra_crossing_good
    WORN ->      R.string.quest_wdm_zebra_crossing_worn
    DISCOMFORTABLE ->      R.string.quest_wdm_zebra_crossing_discomfortable
    HAZARDOUS ->      R.string.quest_wdm_zebra_crossing_hazardous
}

private val WDMZebraCrossing.iconResId: Int get() = when (this) {
    NONE ->  R.drawable.wdm_default_photo
    GOOD -> R.drawable.wdm_zebra_crossing_good
    WORN ->      R.drawable.wdm_zebra_crossing_worn
    DISCOMFORTABLE ->      R.drawable.wdm_zebra_crossing_discomfortable
    HAZARDOUS ->      R.drawable.wdm_zebra_crossing_hazardous
}
