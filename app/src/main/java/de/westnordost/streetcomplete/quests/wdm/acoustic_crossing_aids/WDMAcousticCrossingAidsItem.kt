package de.westnordost.streetcomplete.quests.wdm.acoustic_crossing_aids

import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.quests.wdm.acoustic_crossing_aids.WDMAcousticCrossingAids.NONE
import de.westnordost.streetcomplete.quests.wdm.acoustic_crossing_aids.WDMAcousticCrossingAids.GOOD
import de.westnordost.streetcomplete.quests.wdm.acoustic_crossing_aids.WDMAcousticCrossingAids.WORN
import de.westnordost.streetcomplete.quests.wdm.acoustic_crossing_aids.WDMAcousticCrossingAids.DISCOMFORTABLE
import de.westnordost.streetcomplete.quests.wdm.acoustic_crossing_aids.WDMAcousticCrossingAids.HAZARDOUS
import de.westnordost.streetcomplete.view.image_select.Item

fun WDMAcousticCrossingAids.asItem() = Item(this, iconResId, titleResId)

private val WDMAcousticCrossingAids.titleResId: Int get() = when (this) {
    NONE ->  R.string.quest_wdm_acoustic_crossing_aids_none
    GOOD -> R.string.quest_wdm_acoustic_crossing_aids_good
    WORN ->      R.string.quest_wdm_acoustic_crossing_aids_worn
    DISCOMFORTABLE ->      R.string.quest_wdm_acoustic_crossing_aids_discomfortable
    HAZARDOUS ->      R.string.quest_wdm_acoustic_crossing_aids_hazardous
}

private val WDMAcousticCrossingAids.iconResId: Int get() = when (this) {
    NONE ->  R.drawable.wdm_default_photo
    GOOD -> R.drawable.wdm_acoustic_crossing_aids_good
    WORN ->      R.drawable.wdm_default_photo
    DISCOMFORTABLE ->      R.drawable.wdm_acoustic_crossing_aids_discomfortable
    HAZARDOUS ->      R.drawable.wdm_default_photo
}
